/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.engine;

import org.semanticweb.yars.nx.Node;

/**
 *
 * @author sydgillani
 */
public class Triple {

    public Triple(Node[] nxx, int type) {
        this.nxx = nxx;
        this.type = type;
    }

    public Node[] getNxx() {
        return nxx;
    }

    public void setNxx(Node[] nxx) {
        this.nxx = nxx;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    private  Node[] nxx;
    private int type;
}
