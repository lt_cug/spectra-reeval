/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package edu.cgpm.engine;


import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.Automata;
import edu.cgpm.graph.automata.AutomataState;
import edu.cgpm.qj.queryJoins.QueryProcessorCycle;
import edu.cgpm.summarygraph.SummaryGraphGenerator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sydgillani
 */



public class CGPMEngine   implements Runnable{
    protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(CGPMEngine.class);
 
    private final Automata _automata;
   
    private final long TIME_IN_SECONDS;
    
    
    private long START_OF_WINDOW;
    
    private long REAL_TIME;
  
    //  private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(IncrementalEngine .class.getName());
    private  Thread writingThread;

    
  protected LinkedBlockingQueue<Triple> eventQueue = null;
  
  public final LinkedBlockingQueue<String> resultqueue;
  
    private final DictionaryOpImpl _dictImpl;
    
  
    
   private CountDownLatch latch;

   

   
  private Multimap<Long,AutomataState> _predMap;
    
    
    private final SummaryGraphGenerator _summarygraph;
    private final QueryProcessorCycle _queryProc;
   //  private final QueryProcessor _simpleQueryProc;
       private int counter;

      private FinalView[] _FV;
      private final int proccType;
    
    public CGPMEngine (LinkedBlockingQueue<Triple> q, int counter, int eventType, long window, Automata a,DictionaryOpImpl dictImpl){
        
     
        this.eventQueue=q;
        _summarygraph=new SummaryGraphGenerator();
        REAL_TIME = System.currentTimeMillis();
        proccType=eventType;
        this.counter=counter;
        this._automata=a;
        this._dictImpl=dictImpl;
        
       TIME_IN_SECONDS= window *1000;
     
        _predMap =  HashMultimap.create();
        this.resultqueue =new LinkedBlockingQueue<>();
       //_simpleQueryProc = new   QueryProcessor ();
       
       _queryProc=new QueryProcessorCycle(resultqueue);
      //  final ResultWriter resultWriter = new ResultWriter(1, resultqueue);
	//	this.writingThread = new Thread(resultWriter);
	//	writingThread.setName("QPWriter" + 1);
	//	writingThread.start();
    }
    
   
   
     
    public void fillPredicateMap(){
        for(AutomataState s:_automata.getStates()){
            _predMap.put(s.getRule().getPredicate().getMappedValue(), s);
        }
        
         _FV = new FinalView[_automata.getStates().size()];
       
       for(int i=0;i<_FV.length;i++){
           _FV[i]= new FinalView();
       }
    }
    
    
   @Override
    public void run() {
        fillPredicateMap();
        int count =0;
        int timer=0;
        START_OF_WINDOW = 0;
     //    Node[] nxx = null;
        logger.info("Starting query processor ...");
//        
//        while(parser.hasNext()){
//              nxx = parser.next();
//                _summarygraph.summaryGraphGeneration(_automata,nxx, _dictImpl, timer, _predMap);
//                count++;
//                
//                if(count == this.counter){
//                   
//                    
//                 _queryProc.runQueryProcessing(_automata.getStates(), _dictImpl,  _summarygraph.getChange(),_FV,proccType);
//                   /// _queryProc.runFirstPhase(_automata.getStates(), _dictImpl, _summarygraph.getChange(), _FV, 0, 0, 0);
//                    _summarygraph.setChange(0);
//                    
//                   // checkWindow(System.currentTimeMillis());
//                    
//                    // this.counter=3;
//                    
//                    count=0;
//                }
//        }
        for(;;){
            
            Triple graphEdge=null;
            try {
                graphEdge = this.eventQueue.take();
            } catch (InterruptedException ex) {
                Logger.getLogger(CGPMEngine.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if(graphEdge.getType()==1){
                
             /**
              * First Check the Window and then send the query, if the new event is outside the window then remove the
              * triples from earlier windows, i.e. from FV and C of each state.
              */
                
                _summarygraph.summaryGraphGeneration(_automata,graphEdge.getNxx(), _dictImpl, timer, _predMap);
                count++;
                
                if(count == this.counter){
                    
                   
                    _queryProc.runQueryProcessing(_automata.getStates(), _dictImpl,  _summarygraph.getChange(),_FV,proccType);
                   
                    _summarygraph.setChange(0);
                    
                  //  checkWindow(System.currentTimeMillis());
                    
                    // this.counter=3;
                    
                    count=0;
                    
                    
                ///send for processing
            }
            }else{
                  logger.info("Finshed Query Processing....");
                break;
            }   
        }
        
         this.finish();
       
    }
    
    
    
    public void checkSize(){
        for(AutomataState s:_automata.getStates()){
            System.out.println("State ID: "+s.getStateId());
            System.out.println("Size: "+s.getRule().getC2().size());
            
        }
        System.out.println("############");
    }

public void checkWindow(long currtime){
    
   long timeDiff= currtime-REAL_TIME;
    if(timeDiff - START_OF_WINDOW >= TIME_IN_SECONDS){ //Window has expired
        
      //  System.out.println("Time elapsed "+ timeDiff);
        ///remove everything from the C
        refreshTables();
        ///remove everything from the FV
        refreshFT();
        this._dictImpl.refresh();
        
        START_OF_WINDOW = 0;
        
        REAL_TIME=currtime;
    }
}

public void refreshFT(){
 
       
       for(int i=0;i<_FV.length;i++){
           
           if(!_FV[i].getR().isEmpty()){
           _FV[i].getR().clear();
           _FV[i].getTimeSPpairs().clear();
           _FV[i].getrIndex().clear();
           _FV[i].getrIndexUpdated().clear();
           }
           }
}
public void refreshTables(){
    for(AutomataState s:_automata.getStates()){
        if(  !s.getRule().getC2().isEmpty()){
             s.getRule().getC2().clear();
        }
            
           
    }
}
   public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }
protected void finish() {
		// Notify finish time
		
		// Send poison pill to QPWriter
//		this.resultqueue.add("DONE");
//		// Ensure that QPWriter finishes
//		try {
//			this.writingThread.join();
//		} catch (final InterruptedException e) {
//			e.printStackTrace();
//		}
                
                 logger.info("Finsed Writing Results...");
		// Decrease latch count
		latch.countDown();
	}

}
