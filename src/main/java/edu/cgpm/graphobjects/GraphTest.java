/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.graphobjects;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

/**
 *
 * @author sydgillani
 */
public class GraphTest {

    public  static void main(String args[]) {
        IGraph<String> airlineGraph = new Graph<String>();
        
        buildGraph(airlineGraph);
        //System.out.println(airlineGraph);
        
       testEdges(airlineGraph);
        System.out.println();
        
        //testDFS(airlineGraph);
   // testPath(airlineGraph);
   
    
    }
    
    public static void testEdges(IGraph<String> g) {
        //System.out.println("Edge between ORD and LAX? " + g.hasEdge("ORD", "LAX"));
       // System.out.println("Paths "+ g.getDFSPath("V0", "V10"));
        
       
        
      //  g.deleteVertix("V11");
       String k="V1";
         List<String> path = new ArrayList<>();
        System.out.println(g.getDFSResult(k, path));
       //   System.out.println("Edge between ORD and LAX? " + g.getTheEdgeList("LAX"));
       // System.out.println("Edge between JFK and SFO: " + g.getEdge("JFK", "SFO"));
        
//        System.out.println("Edge between MIA and LAX? " + g.hasEdge("MIA", "LAX"));
//        System.out.println("Edge between MIA and LAX: " + g.getEdge("MIA", "LAX"));      
//        
//        System.out.println("Edge between MIA and ORD? " + g.hasEdge("MIA", "ORD"));
//        System.out.println("Edge between MIA and ORD: " + g.getEdge("MIA", "ORD"));
//        
//        System.out.println("Edge between SFO and DFW? " + g.hasEdge("SFO", "DFW"));
//        System.out.println("Edge between SFO and DFW: " + g.getEdge("SFO", "DFW"));            
    }
    
    public static void testDFS(IGraph<String> g) {
        System.out.println("Path between BOS and LAX? " + g.hasPath("BOS", "LAX"));
        System.out.println("Edge between BOS and LAX: " + g.getDFSPath("BOS", "LAX"));
        
        System.out.println("Path between ORD and LAX? " + g.hasPath("ORD", "LAX"));
        System.out.println("Edge between ORD and LAX: " + g.getDFSPath("ORD", "LAX"));        
        
        System.out.println("Path between SFO and LAX? " + g.hasPath("SFO", "LAX"));
        System.out.println("Edge between SFO and LAX: " + g.getDFSPath("SFO", "LAX"));         
    }
    
    
    
    public static void testPath(IGraph<String> g){
         List<String> path =g.getDFSPath("DFW", "JFK");
        System.out.println(path);
        
        path =g.getDFSPath("JFK", "");
           System.out.println(path);
    }
    
    public static void buildGraph(IGraph<String> g) {
        g.addVertex("V0");
        g.addVertex("V1");
        g.addVertex("V2");
        g.addVertex("V3");
        g.addVertex("V4");
        g.addVertex("V5");
        g.addVertex("V6");
        g.addVertex("V7");
        g.addVertex("V8");
        g.addVertex("V9");
        g.addVertex("V10");
        g.addVertex("V11");
        g.addVertex("V12");
          g.addVertex("V13");
       
        
        
        
        //////////
        
//        G.addEdge(0, 4);
//        G.addEdge(1, 5);
//         G.addEdge(0, 2);
//          G.addEdge(1, 3);
//           G.addEdge(6, 9);
//            G.addEdge(7, 8);
//            
//             G.addEdge(9, 12);
//              G.addEdge(8, 10);
//               G.addEdge(8, 11);
//               
//               
//                G.addEdge(12, 5);
//                
//                 G.addEdge(10, 4);
//                 
//                  G.addEdge(10, 4);
        
        
        
        //////////
        
        g.addEdge("V1", "V2");
        
         g.addEdge("V3", "V4");
         
          g.addEdge("V2", "V5");
          
           g.addEdge("V4", "V6");
           
         
            
             g.addEdge("V5", "V12");
             
              g.addEdge("V5", "V13");
               g.addEdge("V6", "V11");
               
                g.addEdge("V7", "V10");
                
                 g.addEdge("V8", "V9");
                 
                  g.addEdge("V1", "V7");
                   g.addEdge("V3", "V8");
                    g.addEdge("V9", "V11");
                     g.addEdge("V9", "V12");
                      g.addEdge("V10", "V13");
         
         
         
         
        
           
         //g.addEdge("ORD", "LAX", 1500);
//        g.addEdge("DFW", "LAX", 800);
//        g.addEdge("DFW", "ORD", 600);        
//        g.addEdge("DFW", "SFO", 1100);
//        g.addEdge("DFW", "JFK", 950);        
//        g.addEdge("ORD", "DFW", 600);
//        g.addEdge("MIA", "DFW", 1200);
//        g.addEdge("MIA", "LAX", 2800);  
//        g.addEdge("JFK", "MIA", 1300);
//        g.addEdge("JFK", "SFO", 3600);
//        g.addEdge("JFK", "BOS", 300);
//        g.addEdge("BOS", "JFK", 300);
//        g.addEdge("BOS", "MIA", 1500);  
       
    }
}