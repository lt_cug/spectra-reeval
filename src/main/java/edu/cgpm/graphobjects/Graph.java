package edu.cgpm.graphobjects;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import java.util.*;
/**
 *
 * @author sydgillani
 */

public class Graph<V> implements IGraph<V> {
    private Map<V, List<Edge<V>>> adjacencyList;  // [vertices] -> [edge]
    Multimap<V,V> tg =ArrayListMultimap.create();
 
    private Map<V, VertexInfo<V>> vertexInfo;     // [vertex] -> [info]
    
     private Map<V, Boolean> marked;     // [vertex] -> [info]
   
    
    public Graph() {
        this.adjacencyList = new HashMap<V, List<Edge<V>>>();
        this.vertexInfo = new HashMap<V, VertexInfo<V>>();
        
        this.marked= new HashMap<>();
    }
    
    public void addVertex(V v) {
        if (v == null) {
            throw new IllegalArgumentException("null");
        }        
        
        adjacencyList.put(v, new ArrayList<Edge<V>>());
        vertexInfo.put(v, new VertexInfo<V>(v));
    }
    
    public void addEdge(V from, V to) {
        
        tg.put(from, to);
        tg.put(to, from);
        
        List<Edge<V>> edgeList = adjacencyList.get(from);
        if (edgeList == null) {
            throw new IllegalArgumentException("source vertex not in graph");
        }
        
        Edge<V> newEdge = new Edge<V>(from, to);
       edgeList.add(newEdge);
        //Edge<V> newEdge2 = new Edge<V>(to, from, weight);
        //edgeList.add(newEdge2);
        addEdge_Second(to,from);
    }
    
       public void addEdge_Second(V from, V to) {
        List<Edge<V>> edgeList = adjacencyList.get(from);
        if (edgeList == null) {
            throw new IllegalArgumentException("source vertex not in graph");
        }
        
        Edge<V> newEdge = new Edge<V>(from, to);
       edgeList.add(newEdge);
        //Edge<V> newEdge2 = new Edge<V>(to, from, weight);
        //edgeList.add(newEdge2);
    }
       
       public void deleteVertix(V v){
           ////delete it from the vertix info list
           ///delete it from the edge list
           
           this.adjacencyList.remove(v);
           this.vertexInfo.remove(v);
       }
    
    
       
       public List<Edge<V>> getTheEdgeList(V from){
           // List<Edge<V>> edgeList = adjacencyList.get(from);
            
           return adjacencyList.get(from);
       }

    public boolean hasEdge(V from, V to) {
        return getEdge(from, to) != null;
    }

    public Edge<V> getEdge(V from, V to) {
        List<Edge<V>> edgeList = adjacencyList.get(from);
        if (edgeList == null) {
            throw new IllegalArgumentException("source vertex not in graph");
        }
        
        for(Edge<V> e : edgeList) {
        	if (e.to.equals(to)) {
        		return e;
        	}
        }
        
        return null;
    }

    public boolean hasPath(V v1, V v2) {
        return getDFSPath(v1, v2) != null;
    }

    public List<V> getDFSPath(V v1, V v2) {
        clearVertexInfo();
        
        List<V> path = new ArrayList<V>();
        getDFSPath(v1, v2, path);
        
        if (path.isEmpty()) {
            return null;
        } else {
            return path;
        }
    }
    
    public List<V> gettheRList(V v1){
        clearVertexInfo();
        
        List<V> path = new ArrayList<V>();
        
        System.out.println("Edge List"+ getTheEdgeList(v1));
        for(Edge<V> e:getTheEdgeList(v1)){
            this.getDFSPath(v1, e.to, path);
        }
        return path;
    }
    
    
    
    public List<V> getResult2(V v1, List<V> path){
    
  test3(v1,path);
    return  null;
    }
    
    
       public void test3(V v1,List<V> path){
        Stack<V> stack = new Stack<>();
        stack.push(v1);
       // path.add(v1);
        while(!stack.empty()){
            V v= stack.pop();
            
            if(!marked.containsKey(v)|| !marked.get(v)){
                  this.marked.put(v, Boolean.TRUE);
                  path.add(v);
              //    List<Edge<V>> edges = this.adjacencyList.get(v);
                  
                   for (V v2 : this.tg.get(v)) {
                       stack.push(v2);
                       
                   }
                  
            }
            
        }
      
        System.out.println(path); 
        
    }
    
    
    
    
    public void test2(V v1,List<V> path){
        Stack<V> stack = new Stack<>();
        stack.push(v1);
       // path.add(v1);
        while(!stack.empty()){
            V v= stack.pop();
            
            if(!marked.containsKey(v)|| !marked.get(v)){
                  this.marked.put(v, Boolean.TRUE);
                  path.add(v);
                  List<Edge<V>> edges = this.adjacencyList.get(v);
                  
                   for (Edge<V> e : edges) {
                       stack.push(e.to);
                       
                   }
                  
            }
            
        }
      
        System.out.println(path); 
        
    }
    
    
  public List<V> test(V v1, List<V> path){
      path.add(v1);
        
        this.marked.put(v1, Boolean.TRUE);
        
         
        List<Edge<V>> edges = this.adjacencyList.get(v1);
        
        
        for (Edge<V> e : edges) {
        	//VertexInfo<V> vInfo2 = vertexInfo.get(e.to);
        	if (!this.marked.get(e.to)) {
        		getDFSResult(e.to, path);
                if (path.get(path.size() - 1).equals(v1)) {
                    return path;
                }
        	}
        }
        
       // path.remove(v1);
        return path;
  }
       
  
  
  public void getDFStest(V v,Multimap<V,V> list ){
      for(V key: list.keySet()){
          this.addVertex(key);
          
      }
       for(V key: list.keySet()){
          for(V val:list.get(key)){
               this.addEdge(key, val);
          }
          
      }
      
         
      
      ///now send the V to get the paths
      List<V> path =new ArrayList<>();
      getDFSResult(v,  path);
      System.out.println("Paths are" + path);
  }
  
  
    public List<V> getDFSResult(V v1, List<V> path) {
        path.add(v1);
        VertexInfo<V> vInfo = vertexInfo.get(v1);
        vInfo.visited = true;
        //this.marked.put(v1, Boolean.TRUE);
        
       // if (v1.equals(v2)) {
        	//return path;
       // }
        
        List<Edge<V>> edges = this.adjacencyList.get(v1);
        
        
        for (Edge<V> e : edges) {
        	VertexInfo<V> vInfo2 = vertexInfo.get(e.to);
        	if (!vInfo2.visited) {
        		getDFSResult(e.to, path);
                if (path.get(path.size() - 1).equals(v1)) {
                    return path;
                }
        	}
        }
        
       // path.remove(v1);
        return path;
    }
 
    
    public List<V> getDFSPath(V v1, V v2, List<V> path) {
        path.add(v1);
        VertexInfo<V> vInfo = vertexInfo.get(v1);
        vInfo.visited = true;
        
        if (v1.equals(v2)) {
        	return path;
        }
        
        List<Edge<V>> edges = this.adjacencyList.get(v1);
        
        
        for (Edge<V> e : edges) {
        	VertexInfo<V> vInfo2 = vertexInfo.get(e.to);
        	if (!vInfo2.visited) {
        		getDFSPath(e.to, v2, path);
                if (path.get(path.size() - 1).equals(v2)) {
                    return path;
                }
        	}
        }
        
        path.remove(v1);
        return path;
    }
    
    public String toString() {
        Set<V> keys = adjacencyList.keySet();
        String str = "";
        
        for (V v : keys) {
            str += v + ": ";
            
            List<Edge<V>> edgeList = adjacencyList.get(v);
            
            for (Edge<V> edge : edgeList) {
                str += edge + "  ";
            }
            str += "\n";
        }
        return str;
    }
    
    protected final void clearVertexInfo() {
        for (VertexInfo<V> info : this.vertexInfo.values()) {
            info.clear();
        }
    }   
    
       public Map<V, List<Edge<V>>> getAdjacencyList() {
        return adjacencyList;
    }
}