/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package edu.cgpm.UI;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.query.helpers.QueryDiscriptorOptimised;
import edu.cgpm.query.parser.QueryParser;
import edu.cgpm.engine.CGPMEngine;
import edu.cgpm.engine.StreamHandler;
import edu.cgpm.engine.Triple;
import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import javax.xml.datatype.DatatypeConfigurationException;
import org.antlr.runtime.RecognitionException;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.semanticweb.yars.nx.parser.NxParser;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sydgillani
 */
public class MainUI {
    
     protected final static org.slf4j.Logger logger = LoggerFactory.getLogger(MainUI.class);
    public static void main(String args[]) throws RecognitionException, ParseException, DatatypeConfigurationException, Exception{
        
        
        
        Options options = new Options();
        
        
        Option help = new Option("h", "help", false, "show help.");
       
        
        Option inputFile = new Option( "f","file",true, "Input Stream File in NTriple format" );
       //; inputFile.setRequired(true);
        
        Option query = new Option( "q","query",true, "Input Query File" );
       // query.setRequired(true);
        Option processType=  new Option("et", "evalType", true,"Evaluation Type, 1: For Event-based Evalution, 2: For Incremental Evalutaion" );
       // processType.setRequired(true);
        Option window=  new Option( "w","window",true, "Window in Seconds" );
       // window.setRequired(true);
        Option eventBoundry =  new Option("es", "eventSize",true, "# of Triples in each Event" );
       // eventBoundry.setRequired(true);
        options.addOption(help);
        options.addOption(inputFile);
        options.addOption(query);
        options.addOption(processType);
        
        options.addOption(window);
        
        options.addOption(eventBoundry);
        //////////////////////////////////////
        
        String f=null;
        String q=null;
        long w=0L;
        int es=0;
        
        int et=0;
        
        FileInputStream is =null;
        
     
        
        /////////////////////////////////////
           
  try{
        CommandLineParser parserCLI = new BasicParser();
        org.apache.commons.cli.CommandLine line = parserCLI.parse( options, args );
        
       
      
        
        if( line.hasOption( "h" ) ) {
            // initialise the member variable
            // String file = line.getOptionValue( "buildfile" );
            help(options);
        }
        
        if(line.hasOption( "q" ) ){
            q= Files.toString(new File(line.getOptionValue("q")), Charsets.UTF_8);
        }
        
        if(line.hasOption("et")){
            et =Integer.parseInt(line.getOptionValue("et"));
        }
        
        if(line.hasOption("es")){
            es =Integer.parseInt(line.getOptionValue("es"));
        }
        
        if(line.hasOption("w")){
            w= Integer.parseInt(line.getOptionValue("w"));
        }
        
        
        if(line.hasOption( "f" ) ){
            f=line.getOptionValue("f");
            try{
            
              is=new FileInputStream(f);
            }catch(Exception ex){
                System.out.println("Cannot find the file, please check your path again");
                System.exit(0);
            }
        }
      
              }catch(Exception e){
            System.out.println("Error in Parsing the Arguments of the Command Line, please check the arguments or use -h for Help");
              System.exit(0);
        }
      
  
  if(f==null && w==0L && es==0 && q==null ){
      
        System.out.println("All the arguments are not provided, please check the arguments or use -h for Help");
      
      System.exit(0);
  }
    if(!(et==1 ||et==2)){
         System.out.println("All the arguments are not provided, please check the arguments or use -h for Help");
      
      System.exit(0);
    }
  
        //////////////////////////////////////
        
        
        LinkedBlockingQueue<Triple> queue = new LinkedBlockingQueue<>();
     
        
     
        NxParser parser = new NxParser(is);
        
        StreamHandler p = new StreamHandler(queue,"first", 0,parser);
        
        DictionaryOpImpl dictimpl = new DictionaryOpImpl();
        
        final QueryDiscriptorOptimised descriptor = QueryParser.parse(q, dictimpl,"");
        
        
        CGPMEngine engine = new CGPMEngine(queue, es, et, w, descriptor.getNfaData().getAutomta(),dictimpl);
       
        final CountDownLatch latch = new CountDownLatch(2);
        engine.setLatch(latch);
        p.setLatch(latch);
        logger.info("Starting the Stream .....");
        new Thread(p).start();
        //logger.info("Starting the Stream .....");
        new Thread (engine).start();
        
        
        
        
        latch.await();
        
        
        
    }
    
    
    private static void help(Options opt) {
        // This prints out some help
        HelpFormatter formater = new HelpFormatter();
        
        formater.printHelp("java -cp ApacheCommonsCLI.jar", opt);
        System.exit(0);
        
//          
//        String querys=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                
//                " prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
//                " prefix ub: <http://swat.cse.lehigh.edu/onto/univ-bench.owl#> "+
//                "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                + " SELECT ?tag "+
//                
//                
//                "WHERE  { "
//                
//                +"  ?y ub:teacherOf ?z. " + //0
//                "    ?x ub:takesCourse ?z.   " +//3
//                
//                
//                "       ?z rdf:type ub:Course.  " +//1
//                
//                " ?x ub:advisor ?y. " + //5
//                
//                "   ?y rdf:type ub:FullProfessor. " +//2
//                
//                
//                "       ?x rdf:type ub:UndergraduateStudent. " + //4
//                
//                
//                
//                
//                
//                
//                
//                
//                "        } ";   ///1
//        
//        
//        
        
    }
}
