/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.qj.interfaces;

import com.jcwhatever.nucleus.collections.MultiBiMap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.datastructure.MultiBidirectionalIndex;
import edu.cgpm.datastructure.SO;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.AutomataState;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sydgillani
 */
public interface IncQueryProccInterface {

    void case1(List<AutomataState> incList, FinalView[] _FV, DictionaryOpImpl dic, long timestamp);

    void case2(List<AutomataState> incList, FinalView[] _FV);

    boolean case2Checker(List<AutomataState> a, long pIndex, long cIndex);

    Set<SO> extractioFromrIndex(AutomataState s, MultiBiMap<Long, SO> table, long index, long pIndex, FinalView _FV, long timestamp, Set<SO> nSP);

    int getTheCuttingIndex(List<Long> timeList, long tb);

    void incrementalOOJoinStage(AutomataState currState, FinalView tJoin, int caseType);

    void incrementalOSJoinStage(AutomataState currState, FinalView tJoin, int caseType);

    void incrementalSOJoinStage(AutomataState currState, FinalView tJoin, int caseType);

    void incrementalSSJoinStage(AutomataState currState, FinalView tJoin, int caseType);

    ////Whats happening over here
    boolean indexChecker(long index, FinalView[] _FV);

    void indexExtraction(List<AutomataState> a, FinalView[] _FV, DictionaryOpImpl dic, long timestamp);

    void indexExtractionCase2(List<AutomataState> a, FinalView[] _FV, DictionaryOpImpl dic, long timestamp);

    void joinWithFV(AutomataState currState, List<AutomataState> a, FinalView[] _FV, int caseType);

    void ooJoinOptimisedForCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1);

    void ooJoinOptimisedForRC2(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3);

    void ooJoinOptimisedForRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3);

    void osJoinOptimisedForCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1);

    void osJoinOptimisedForRC2(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4);

    void osJoinOptimisedForRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4);

    void printOut(List<Set<SO>> setList, DictionaryOpImpl dic);

    void removalProcess(List<Long> timeList, FinalView _fv, int index);

    void removeMatchesFromFV(List<Long> timeList, long tb, FinalView _fv);

    void resultExtraction(long index, long pIndex, List<AutomataState> a, List<Set<SO>> setList, FinalView[] _FV, long timestamp, int seq);

    void resultExtractionDEPRIICATED(long index, List<AutomataState> a, List<Set<SO>> setList, FinalView[] _FV, long timestamp, int seq);

    //It takes the automata
    void run(List<AutomataState> aList, FinalView[] _FV, DictionaryOpImpl dic, long timestamp, long range, long step);

    void setChange(List<AutomataState> incList);

    void soJoinOptimisedForCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1);

    void soJoinOptimisedForRC2(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3);

    void soJoinOptimisedForRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3);

    void ssJoinOptimisedForCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1);

    void ssJoinOptimisedForRC2(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4);

    void ssJoinOptimisedForRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4);

    void windowUpdate(long range, long step, FinalView[] _FV);
    
}
