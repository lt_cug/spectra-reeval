/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.qj.interfaces;

import com.google.common.collect.Multimap;
import com.jcwhatever.nucleus.collections.MultiBiMap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.datastructure.Indexes;
import edu.cgpm.datastructure.MultiBidirectionalIndex;
import edu.cgpm.datastructure.SO;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.AutomataState;
import edu.cgpm.rulesmodel.Dependability;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 *
 * @author sydgillani
 */
public interface QueryProcCycleInterface {

    void eachJoinResult(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _c2, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    Set<SO> extractResults(AutomataState s, AutomataState js, Indexes index, Set<SO> set, FinalView[] _FV, long timestamp);

    void firstStage(MultiBiMap result, Indexes index, AutomataState s, FinalView[] _FV, long timestamp, Set<SO> set);

    void generalStage(int size, List<AutomataState> aStates, DictionaryOpImpl dic, FinalView[] _FV, long timestamp, long range, long step);

    void graphPairResult(Multimap<Long, Long> m);
  
    void incrementalOOJoinStage(AutomataState currState, AutomataState joinState, Dependability dd, Dependability jdd);

    ///////////////////////######################################################################////////////////////////////////////////
    void incrementalOSJoinStage(AutomataState currState, AutomataState joinState, Dependability dd, Dependability jdd);
    ///////////////////////######################################################################////////////////////////////////////////

    ///////////////////////######################################################################////////////////////////////////////////
    void incrementalSOJoinStage(AutomataState currState, AutomataState joinState, Dependability dd, Dependability jdd);

    void incrementalSSJoinStage(AutomataState currState, AutomataState joinState, Dependability dd, Dependability jdd);

    void initialStage(List<AutomataState> aStates, DictionaryOpImpl dic, FinalView[] _FV, long timestamp, long range, long step);

 
    void ooHashJoinCC(MultiBiMap<Long, Long> _c1, MultiBiMap<Long, Long> _c2, MultiBidirectionalIndex _r1, MultiBidirectionalIndex _r2, long index, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void ooHashJoinRC(MultiBiMap<Long, Long> _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r3, MultiBidirectionalIndex _r4, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void ooHashJoinRC2(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r3, MultiBidirectionalIndex _r4, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    ///////////////////////######################################################################////////////////////////////////////////
    ///Modify it
    //ISSUE: 1
    void osHashJoinCC(MultiBiMap<Long, Long> _c1, MultiBiMap<Long, Long> _c2, MultiBidirectionalIndex _r1, MultiBidirectionalIndex _r2, long index, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    //Edit 4
    void osHashJoinRC(MultiBiMap<Long, Long> _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r4, MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    ///Edit 6, Sixth Join
    ///Issue 6:
    void osHashJoinRC2(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r4, MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void outputResultsAll(List<AutomataState> a, DictionaryOpImpl dic, FinalView[] _FV, long timestamp);

    void refreshStructures(List<AutomataState> aStates);

    /////////////////////////////////////////////////////////////////////////
    void removeOldTriples(List<AutomataState> as, long time, long range, long step);

    void runFirstPhase(Collection<AutomataState> a, DictionaryOpImpl dic, int change, FinalView[] _FV, long timestamp, long range, long step);

    void secondStage(AutomataState s, AutomataState js, Set<SO> newSet, Set<SO> set, FinalView[] _FV, long timestamp);

    ///First Join
    void soHashJoinCC(MultiBiMap<Long, Long> _c1, MultiBiMap<Long, Long> _c2, MultiBidirectionalIndex _r1, MultiBidirectionalIndex _r2, long index, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    // Third Join
    ///Issue: 3
    void soHashJoinRC(MultiBiMap<Long, Long> _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r3, MultiBidirectionalIndex _r4, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void soHashJoinRC2(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r3, MultiBidirectionalIndex _r4, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);
    ///////////////////////######################################################################////////////////////////////////////////

    ///Edit 1
    void ssJoinOptimisedForCC(MultiBiMap<Long, Long> _c1, MultiBiMap<Long, Long> _c2, MultiBidirectionalIndex _r1, MultiBidirectionalIndex _r2, long index2, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    ///Second and Fourth, and fifth Join
    /// chagen the multi BiMap tp multimap
    ///ISSUE: 2,4, 5
    void ssJoinOptimisedForRC(MultiBiMap<Long, Long> _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r4, MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void ssJoinOptimisedForRC2(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r4, MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void tpJoinAutomataStage(List<AutomataState> automata);

    void tripleJoinStage(AutomataState currState, List<AutomataState> a);

    void tripleRemoval(AutomataState s, long tb);
    /////////////////////////////////////////////////////////////////////////
    
}
