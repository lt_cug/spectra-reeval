/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

package edu.cgpm.qj.queryJoins;

import edu.cgpm.datastructure.Indexes;
import edu.cgpm.datastructure.SO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author sydgillani
 */
public class Utilities {
    
    
    public int stop;
   public  boolean checker;
    // private boolean on2ndState=true;
    public int stateID;
    // private ArrayList<Long> indexes= new ArrayList<>();
    public    List<Set<SO>> setList;
    // private DictionaryOpImpl dic; ///delete it later
    public int breakpt;
    //  int test=0;
    //  public boolean resultHandler=false;
    public final ArrayList<Long> indexes;
    
    public  Set<SO> visited;
   public ArrayList<Integer> _joinOrder ;
    
    public long indexCount;
    
    public long perTableIndex;
    
   public  Map<Integer, Set<SO>> closet;
   public ArrayList<Indexes> parentChild ;
    
    public Utilities(){
        stop=0;
        visited = new HashSet<>();
        checker=false;
        stateID=0;
        closet= new HashMap<>();
        setList= new ArrayList<>();
        breakpt=100000;
        
        indexes = new ArrayList<>();
        _joinOrder = new ArrayList<>();
        
        indexCount=0;
        
        perTableIndex=0;
        
        parentChild = new ArrayList<>();
    }
    public void refresh(){
        
     
        
         _joinOrder.clear();
        perTableIndex=0;
        
        breakpt=100000;
        
        stateID=0;

         stop=0;
            
          checker=false;
    }
    
   
}
