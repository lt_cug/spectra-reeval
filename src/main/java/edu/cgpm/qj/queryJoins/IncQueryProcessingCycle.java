/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.qj.queryJoins;

import edu.cgpm.qj.interfaces.IncQueryProcCycleInterface;
import com.jcwhatever.nucleus.collections.MultiBiMap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.datastructure.Indexes;
import edu.cgpm.datastructure.MultiBidirectionalIndex;
import edu.cgpm.datastructure.SO;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.AutomataState;
import edu.cgpm.rulesmodel.Dependability;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author sydgillani
 */
public class IncQueryProcessingCycle implements IncQueryProcCycleInterface {
    
     
//    private int stop=0;
//    private  boolean checker=false;
//   // private boolean on2ndState=true;
//    private int stateID;
//   // private ArrayList<Long> indexes= new ArrayList<>();
//    private    List<Set<SO>> setList= new ArrayList<>();
//    private DictionaryOpImpl dic; ///delete it later
//    private int breakpt=0;
//    //  int test=0;
    //  public boolean resultHandler=false;
    
   
    
   // private long indexCount;
    
   // private ResultManipulation _reM = new  ResultManipulation();
    
//    private long perTableIndex;
//    
//    //private ArrayList<Indexes> parentChild =new ArrayList<>();
//    
   private int doneIT=0;
//    
//    private int indexLimit;
    //TODO: How to extract the results for the Case 1 and Case 2. Both will have different strategies.
    
    
    ///Result Extractions for Case 1:
    ///Get the smallest index and get the index simply extract it from the list
    
    private Utilities _utils = new Utilities();
    
    @Override
      public void run(List<AutomataState> aList, FinalView[] _FV, DictionaryOpImpl dic, long timestamp, long range, long step, Utilities Util,ResultManipulation _reM) {

          _utils.refresh();
        ///get all the states with non=empty result sets
       // indexLimit = 10000000;
       // stop = 0;
          doneIT = 0;
        //  this.indexCount=indexCount;
        List<AutomataState> incList = aList.stream().filter(x -> x.getEdge().getRreal().getR().size() > 0 || x.getEdge().getrCopied().getR().size() > 0).collect(Collectors.toList()); //this is an extra work
/**
 * There are two main cases, one involves with triple patterns only having the C > 0 and other having R > 0, thus for the case 1, the 
 * incoming new triples within a view is joined with just the FV and it takes its indexes, the updated matches are produced only the index present 
 * in all the views in FV. For the second case, we need to again take the join index of the FV and use it for the storage, the matched results are produced only
 * if the index is present in the all the lists. 
 */
        if (incList.isEmpty()) {                                        ///put the get change later over herer
            incList = aList.stream().filter(x -> x.getRule().getC2().size() > 0 && x.getChange() == 1).collect(Collectors.toList());
            if (!incList.isEmpty()) {
                this.windowUpdate(range, step, _FV, timestamp); ///Delete the older Objects from FV
                case1(incList, _FV, dic, timestamp, _reM);
            }
            ///use a different operation for this type of joins
        } else {
    ///if its a single triple then what the fuck???
            ///now iterate on this list to get the unjoin ones to the FV
            ///Here deltion from the window
            ///take the current time, and implemet the formula as given in the paper
          
         // this.windowUpdate(range, step, _FV,timestamp);
          this.case2(incList, _FV);

            
            if(doneIT ==0){
                ////Release the first join from the first and the second from the last
                incList.get(1).getRule().getDepends().get(0).setJoined(0);
                 this.case2(incList, _FV);
            }
            
            
            if (_utils.stop == 0 && doneIT == 1) {
            //then gather the results and put it in the FV
                 _reM.indexExtraction_Case2(aList, _FV, dic, timestamp, _utils.stateID, Util);
               // indexExtraction_Case2(incList, _FV, dic, timestamp);
            }
        }
    }

   //////////////////////////////////////////////////////////////Result Extraction/////////////////////////////
      
    @Override
       public void case2(List<AutomataState> incList, FinalView[] _FV){
           for (AutomataState s : incList) {
                
              
                joinWithFV(s, incList, _FV, 0);

                if (_utils.stop == 1) {
                    setChange(incList);
                    ///change the set change stuff to zero
                    return;
                }
            }
       }
    
      
      
    @Override
      public void case1(List<AutomataState> incList, FinalView[] _FV, DictionaryOpImpl dic, long timestamp,ResultManipulation _reM) {
        for (AutomataState s : incList) {
            joinWithFV(s, incList, _FV, 1);

            if (_utils.stop == 1) {

                ///set the set change ==0
                setChange(incList);
                return;
            }
        }

     //   indexExtraction(incList, _FV, dic, timestamp);
    _reM.indexExtraction_Case1(incList, _FV, dic, timestamp, _utils.stateID);
    }

    @Override
    public void setChange(List<AutomataState> incList) {
        incList.stream().forEach(x -> x.setChange(0));
    } 
    
    /*
        public void indexExtraction_Case1(List<AutomataState> a, FinalView[] _FV, DictionaryOpImpl dic, long timestamp) {
        Set<Long> keys = null;

        List<Set<SO>> setList = new ArrayList<>();
        // this.stateID=5;
        AutomataState keyState = a.stream().filter(x -> x.getStateId() == this.stateID).findFirst().orElse(null);

        if (keyState == null) {
            System.out.println("");
        } else {

            if (!keyState.getEdge().getRreal().getR().isEmpty()) {

                keys = keyState.getEdge().getRreal().getrIndex().keySet();
                
               

            } else if (!keyState.getEdge().getrCopied().getR().isEmpty()) {

                keys = keyState.getEdge().getrCopied().getrIndex().keySet();
            }

            int seq = 1;
            for (long in : keys) {
                // statesDFS(a.getStates().get(stateID), a, in);
                resultExtraction_Case1(in,in, a, setList, _FV, timestamp, seq);
                seq++;
                
                ///Print out the results only if this index exists in all the views in FV
                
                if(indexChecker(in,_FV)){
                     printOut(setList, dic);
                }
                
               
            }

        }
    }
        
    public boolean indexChecker(long index, FinalView[] _FV){
       for(FinalView fv:_FV){
           if(!fv.getrIndex().containsKey(index)){
               return false;
           }
       }
    
    return true;
    }
    
    
    public void resultExtraction_Case1(long index, long pIndex, List<AutomataState> a, List<Set<SO>> setList, FinalView[] _FV, long timestamp, int seq) {
        Set<SO> nSP;

        setList.clear();
        for (AutomataState s : a) {
            nSP = new HashSet<>();
            if (!s.getEdge().getRreal().getrIndex().isEmpty()) {

             
               
                     setList.add(extractioFromrIndex(s, s.getEdge().getRreal().getrIndex(), index,pIndex, _FV[s.getStateId()], timestamp, nSP));
               
                ///setlist to output the results

            } else {

              
                //   _FV[s.getStateId()].getTimeIndexMap().put(timestamp, index);
                setList.add(extractioFromrIndex(s, s.getEdge().getrCopied().getrIndex(), index,pIndex, _FV[s.getStateId()], timestamp, nSP));
            }

            s.setChange(0);
            if (seq == 1) {
                _FV[s.getStateId()].getTimeList().add(timestamp);
            }
        }

    }
    
    
    
    
    public Set<SO> extractioFromrIndex(AutomataState s, MultiBiMap<Long, SO> table,long index, long pIndex, FinalView _FV, long timestamp, Set<SO> nSP){
           for (Iterator<Map.Entry<Long, SO>> it = table.entries().iterator(); it.hasNext();) {
                    Map.Entry<Long, SO> sp = it.next();
                    if (sp.getKey() == index) {
                        _FV.getR().put(sp.getValue().getSub(), sp.getValue().getPred());
                        _FV.getTimeSPpairs().put(timestamp, sp.getValue()); ////also the time tree will be effective over here
                        _FV.getrIndex().put(pIndex, sp.getValue());

                        s.getRule().getC2().remove(sp.getValue().getSub(), sp.getValue().getPred()); ///remove the timestamp from the 
                       
                         s.getRule().getTimeSOpairs().remove(timestamp, new SO(sp.getValue().getSub(),sp.getValue().getPred()));
                        
                        nSP.add(sp.getValue());///add to FV in the required table
                    }
                }
           
           return nSP;
    }
    

    public void printOut(List<Set<SO>> setList, DictionaryOpImpl dic) {

        Set<List<SO>> result = Sets.cartesianProduct(setList);
        result.forEach((outList) -> {

            outList.forEach((out) -> {
                System.out.print("--> Subject  " + dic.getResourceIncremenal(out.getSub()) + "--> Object " + dic.getResourceIncremenal(out.getPred()));

                 ///   System.out.println("--> "+" "+dic.getResourceIncremenal( out.getPred()) );
                Literal lg = dic.getLiteralIncremental(out.getPred());
                if (lg != null) {
                    System.out.print(lg.getValue().toString());
                }

                System.out.println();
                  //  s.getRule().getC2().remove(out.getSub(), out.getPred());
            });
            System.out.println("######################################");

        });

    }      
        
*/
      
   ////////////////////////////Results Manipulation////////////////////////// 
       
       
       
       
       
       
       
       
       
       
       
       
       
       
    @Override
     public void joinWithFV(AutomataState currState, List<AutomataState> a, FinalView[] _FV, int caseType) {
        for (Dependability dd : currState.getRule().getDepends()) {
            if (dd.getJoined() == 0) {
                ///find the join state
                // AutomataState sJoin = a.getStates().stream().filter(x->x.getStateId()==dd.getDependabilty_id()).findFirst().get();

                //get the join state or the table from the FV list
                FinalView sJoin = null;

                sJoin = _FV[dd.getDependabilty_id()];

                doneIT = 1;

                //Remove the stop ==0, as we need tp join as many as possible
                if (dd.getDependability_On() == 1 && dd.getDependability_part() == 1) { ///stop==0;

                    incrementalSSJoinStage(currState, sJoin, caseType);

                } else if (dd.getDependability_On() == 1 && dd.getDependability_part() == 0) {

                    incrementalOSJoinStage(currState, sJoin, caseType);

                } else if (dd.getDependability_On() == 0 && dd.getDependability_part() == 1) {
                    //  incrementalSOJoin(currState,sJoin,a);

                    //  incrementalSOJoinStage(currState, sJoin,dd,jdd);
                    incrementalSOJoinStage(currState, sJoin, caseType);

                } else if (dd.getDependability_On() == 0 && dd.getDependability_part() == 0) {

                    incrementalOOJoinStage(currState, sJoin, caseType);
                }

                if (_utils.stop == 0 && !currState.getEdge().getRreal().getR().isEmpty()) {
                    ///then get 

                    if (currState.getEdge().getRreal().getParentChild().size() <= _utils.breakpt) {
                        _utils.breakpt = currState.getEdge().getRreal().getParentChild().size();
                        _utils.stateID = currState.getStateId();
                      ///  System.out.println("ID:  " + _utils.stateID);
                    }

                } else if (_utils.stop == 0 && !currState.getEdge().getrCopied().getR().isEmpty()) {
                    if (currState.getEdge().getrCopied().getParentChild().size() <= _utils.breakpt) {
                        _utils.breakpt = currState.getEdge().getrCopied().getParentChild().size();
                        _utils.stateID = currState.getStateId();
                      //  System.out.println("ID:  " + _utils.stateID);
                    }
                }

            }
        }

    }
    
    
    @Override
      public void incrementalSSJoinStage(AutomataState currState, FinalView tJoin, int caseType) {

        if (currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()) {
            ///use cc joins over here

            if (!currState.getRule().getC2().isEmpty()) {
               // this.ssHashJoinCC(currState.getRule().getC2(), tJoin, currState.getEdge().getRreal());
                ssHashJoinCC(currState.getRule().getC2(), tJoin, currState.getEdge().getRreal());
                if (!_utils.checker) {
                    _utils.stop = 1;
                } else {
                    _utils.checker = false;
                }
            }

        } else if (!currState.getEdge().getRreal().getR().isEmpty()) {

            if (caseType == 1) {
              //  ssJoinOptimisedForRC2_Case1(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                
                this.ssHashJoinRC2_Case1(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
            } else {
                this.ssHashJoinRC2(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied(), currState.getParentChild());
               // ssHashJoinRC2(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
            }
            if (_utils.checker) {
                currState.getEdge().getRreal().getR().clear();
                currState.getEdge().getRreal().getrIndexUpdated().clear();
                _utils.checker = false;
            } else {
                _utils.stop = 1;
            }

        } else if (!currState.getEdge().getrCopied().getR().isEmpty()) {

            if (caseType == 1) {
             //   ssJoinOptimisedForRC2_Case1(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                
                 this.ssHashJoinRC2_Case1(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
            } else {
              //  ssHashJoinRC2(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                
                 this.ssHashJoinRC2(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal(), currState.getParentChild());
            }

            if (_utils.checker) {
                _utils.checker = false;
                currState.getEdge().getrCopied().getR().clear();
                currState.getEdge().getrCopied().getrIndexUpdated().clear();
            } else {
                _utils.stop = 1;
            }

        }

    }

    @Override
    public void incrementalOSJoinStage(AutomataState currState, FinalView tJoin, int caseType) {

        if (currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()) {
            ///use cc joins over here

            if (!currState.getRule().getC2().isEmpty()) {
               // this.osJoinOptimisedForCC(currState.getRule().getC2(), tJoin, currState.getEdge().getRreal());
                 this.osHashJoinCC(currState.getRule().getC2(), tJoin,currState.getEdge().getRreal());
                if (!_utils.checker) {
                    _utils.stop = 1;
                } else {
                    _utils.checker = false;
                }
            }

        } else if (!currState.getEdge().getRreal().getR().isEmpty()) {

            if (caseType == 1) {
             //   osJoinOptimisedForRC2_Case1(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                this.osHashJoinRC2_Case1(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                
                
            } else {
               // osJoinOptimisedForRC2(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                 this.osHashJoinRC2(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied(),currState.getParentChild());
            }

            if (_utils.checker) {
                _utils.checker = false;
                if(!currState.getEdge().getRreal().getR().isEmpty()){
                currState.getEdge().getRreal().getR().clear();
                currState.getEdge().getRreal().getrIndexUpdated().clear();
            }
            } else {
                _utils.stop = 1;
            }

        } else {
            if (caseType == 1) {
             //   osJoinOptimisedForRC2_Case1(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
               this.osHashJoinRC2_Case1(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                
            } else {
             //   osJoinOptimisedForRC2(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                this.osHashJoinRC2(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal(),currState.getParentChild());
            }

            if (_utils.checker) {
                _utils.checker = false;
                currState.getEdge().getrCopied().getR().clear();
                currState.getEdge().getrCopied().getrIndexUpdated().clear();
            } else {
                _utils.stop = 1;
            }

        }
    }

    @Override
    public void incrementalSOJoinStage(AutomataState currState, FinalView tJoin, int caseType) {

        if (currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()) {
            ///use cc joins over here

            if (!currState.getRule().getC2().isEmpty()) {
              //  this.soJoinOptimisedForCC(currState.getRule().getC2(), tJoin, currState.getEdge().getRreal());
              this.soHashJoinCC(currState.getRule().getC2(), tJoin, currState.getEdge().getRreal());
                if (!_utils.checker) {
                    _utils.stop = 1;
                } else {
                    _utils.checker = false;
                }
            }

        } else if (!currState.getEdge().getRreal().getR().isEmpty()) {

            if (caseType == 1) {
             //   soJoinOptimisedForRC2_Case1(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                
                this.soHashJoinRC2_Case1(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                
                
                
                
            } else {
            //    soJoinOptimisedForRC2(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                this.soHashJoinRC2(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied(),currState.getParentChild());
            }

            if (_utils.checker) {
                _utils.checker = false;
                currState.getEdge().getRreal().getR().clear();
                currState.getEdge().getRreal().getrIndexUpdated().clear();
            } else {
                _utils.stop = 1;
            }

        } else {

            if (caseType == 1) {
              //  ssJoinOptimisedForRC2_Case1(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                
              this.soHashJoinRC2_Case1(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                
            } else {
            //    ssHashJoinRC2(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                
                
                this.soHashJoinRC2(currState.getEdge().getrCopied(), tJoin,currState.getEdge().getRreal(), currState.getParentChild());
            }

            if (_utils.checker) {
                _utils.checker = false;
                currState.getEdge().getrCopied().getR().clear();
                currState.getEdge().getrCopied().getrIndexUpdated().clear();
            } else {
               _utils. stop = 1;
            }

        }
    }

    @Override
    public void incrementalOOJoinStage(AutomataState currState, FinalView tJoin, int caseType) {

        if (currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()) {
            ///use cc joins over here

            if (!currState.getRule().getC2().isEmpty()) {
              //  this.ooJoinOptimisedForCC(currState.getRule().getC2(), tJoin, currState.getEdge().getRreal());
                this.ooHashJoinCC(currState.getRule().getC2(), tJoin, currState.getEdge().getRreal());
                if (!_utils.checker) {
                    _utils.stop = 1;
                } else {
                    _utils.checker = false;
                }
            }

        } else if (!currState.getEdge().getRreal().getR().isEmpty()) {

            if (caseType == 1) {
              //  ooJoinOptimisedForRC2_Case1(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                
                this.ooHashJoinRC2_Case1(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
            } else {
               // ooJoinOptimisedForRC2(currState.getEdge().getRreal(), tJoin, currState.getEdge().getrCopied());
                
                this.ooHashJoinRC2(currState.getEdge().getRreal(), tJoin,currState.getEdge().getrCopied(), currState.getParentChild());
            }

            if (_utils.checker) {
                _utils.checker = false;
                currState.getEdge().getRreal().getR().clear();
                currState.getEdge().getRreal().getrIndexUpdated().clear();
            } else {
               _utils. stop = 1;
            }

        } else {

            if (caseType == 1) {
              //  ooJoinOptimisedForRC2_Case1(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                
                   this.ooHashJoinRC2_Case1(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
            } else {
              //  ooJoinOptimisedForRC2(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal());
                
                this.ooHashJoinRC2(currState.getEdge().getrCopied(), tJoin, currState.getEdge().getRreal(), currState.getParentChild());
            }

            if (_utils.checker) {
                _utils.checker = false;
                currState.getEdge().getrCopied().getR().clear();
                currState.getEdge().getrCopied().getrIndexUpdated().clear();
            } else {
                _utils.stop = 1;
            }

        }
    }
    
    
    
    
    
    
    @Override
        public void ssHashJoinCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1) {

        ///Take the index from the top
        ///Increment the index
        for (long key : _c1.keySet()) {

            if (_c2.getR().containsKey(key)) {
                _utils.checker = true;

                Set<Long> keyall = _c1.get(key);
                for (long val : keyall) {
                    _r1.getR().put(key, val);

                    SO sp = new SO(key, _c2.getR().getValue(key));
                    for (long in : _c2.getrIndex().getKeys(sp)) {
                        _r1.getrIndex().put(in, new SO(key, val));
                    }

                }

//                Set<Long> r2val=_c2.get(key);
//                for(long val:r2val){
//                    
//                      _r2.getR().put(key, val);
//                     
//                   
//                         for(long kin:this.indexes){
//                             _r2.getrIndex().put(kin, new SO(key,val));
//                         }
//                      
//                     
//                }                
            }
            //
        }

        //  graphPairResult(dd);
    }

    @Override
    public void osHashJoinCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1) {

       //_c1 is the current State
        for (Long o : _c1.keySet()) {//change the order

            if (_c2.getR().containsValue(o)) {
               _utils. checker = true;

                Set<Long> val = _c1.get(o); ///why this
                for (long v : val) {

                    _r1.getR().put(o, v);

                    SO sp = new SO(_c2.getR().getKey(o), o);
                    for (long in : _c2.getrIndex().getKeys(sp)) {

                        _r1.getrIndex().put(in, new SO(o, v));
                    }

                }
             //   System.out.println("Values: "+ _c2.getKeys(index));
                ////////////////////
//                Set<Long> keys =_c2.getKeys(o);  ///change it if its too expensive
//                for(long k:keys){
//                     _r2.getR().put(k, o);   //TODO: should I get all the values
//                     
//                     for(long kin:this.indexes)
//                _r2.getrIndex().put(kin,new SO(k, o));  ///change it later
//                }

            }

        }

   //   this.eachJoinResult(_r1, _r2,_pC,_jpC);
    }

    @Override
    public void soHashJoinCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1) {

         ///C_1 is the current state
        for (Long o : _c1.values()) {//change the order

            if (_c2.getR().containsKey(o)) {
                _utils.checker = true;

               // _r1.getR().put(_c1.getKey(o), o);
                Set<Long> keyall = _c1.getKeys(o);

                for (long ka : keyall) {
                    _r1.getR().put(ka, o);

                    SO sp = new SO(o, _c2.getR().getValue(o));
                    for (long in : _c2.getrIndex().getKeys(sp)) {
                        _r1.getrIndex().put(in, new SO(ka, o));

                    }

                     // this.indexes.add(index);
                    // index++;
                }

//                Set<Long> val=_c2.get(o);
//                for(long v:val){
//                    _r2.getR().put(o,v);
//                    for(long kin:this.indexes)
//                    _r2.getrIndex().put(kin,new SO(o, v));
//                }
                ///get all the values of the with this key
            }

        }

      //  this.eachJoinResult(_r1, _r2,_pC,_jpC);
    }

    @Override
    public void ooHashJoinCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1) {

        for (Long obj : _c1.values()) {
            if (_c2.getR().containsValue(obj)) {

               _utils. checker = true;
               // _r1.getR().put(_c1.getKey(obj), obj);

                Set<Long> keyall = _c1.getKeys(obj);
                for (long key : keyall) {
                    _r1.getR().put(key, obj);

                    SO sp = new SO(_c2.getR().getKey(obj), obj);
                    for (long in : _c2.getrIndex().getKeys(sp)) {
                        _r1.getrIndex().put(in, new SO(key, obj));
                    }

                }

//                Set<Long> keyall2= _c2.getKeys(obj);
//                for(long key:keyall2){
//                    _r2.getR().put(key, obj);
//                   for(long kin:this.indexes)
//                       _r2.getrIndex().put(kin, new SO(key,obj));
//                }
            }

        }

        // this.eachJoinResult(_r1, _r2);
    }

    
    @Override
    public void ooHashJoinRC2(MultiBidirectionalIndex _c1,FinalView _r2,MultiBidirectionalIndex _r3,MultiBiMap<Indexes,Indexes> _pC){
        
        
        for(long obj:_c1.getR().values()){
            if(_r2.getR().containsValue(obj)){
               
                
                
                ///get the keys from c1
                
                Set<Long> keyall=_c1.getR().getKeys(obj);  
                 Set<Long> keyall2= _r2.getR().getKeys(obj);
                
                for(long keys:keyall){ //coming from c
                    
                    
                    SO sp= new SO(keys, obj);
                        
                        
                        Set<Indexes> in=_c1.getrIndexUpdated().getKeys(sp);
                        
                        _r3.getR().put(keys, obj);
                        for(Indexes kin:in){
                            
                            _r3.getrIndexUpdated().put(kin, sp);
                            
                             for(long val:keyall2){
                                 
                                
                                
                                for(long kin2:_r2.getrIndex().getKeys(new SO(val,obj))){
                                    
                                    _r3.getParentChild().put(kin2, kin.getGlobalIndex());
                                 //_pC.put(kin, kin2);
                                }
                                 
//                          
                             }
                        }
                    
                    
                    
                    
                    
                   
                    
                }
                        
               _utils. checker=true;
                ///first get the index from the _r2
         
                
                
                
                
                
            }
            
            
        }
        
        
      // this.eachJoinResult(_r3, _r4);
        
    }
    @Override
     public void soHashJoinRC2(MultiBidirectionalIndex _c1,FinalView _r2,MultiBidirectionalIndex _r3,MultiBiMap<Indexes,Indexes> _pC){
        
        for(long sub:_c1.getR().values()){
            
            if(_r2.getR().containsKey(sub)){
                
                _utils.checker=true;
               Set<Long> keyall=_c1.getR().getKeys(sub);
                Set<Long> valall=_r2.getR().get(sub);
                
                
                for (long key:keyall) {
                    
                            SO sp= new SO(key,sub);
                            
                             Set<Indexes> in=_c1.getrIndexUpdated().getKeys(sp);
                             _r3.getR().put(key, sub);
                             for(Indexes kin:in){
                                 _r3.getrIndexUpdated().put(kin, sp);
                                 
                                 for(long val:valall){
                                     
                                     for(long  kin2:_r2.getrIndex().getKeys(new SO(sub,val))){
                                         
                                         _r3.getParentChild().put(kin2, kin.getGlobalIndex());
                                       
                                     }
                                     
                                     
                                 }
                             }
                             
                }
                
      
                
                
                
     
                
            }
            
        }
       // this.eachJoinResult(_r3, _r4);
    }
    @Override
      public void osHashJoinRC2(MultiBidirectionalIndex _c1, FinalView _r2,MultiBidirectionalIndex  _r4,MultiBiMap <Indexes,Indexes> _pC){
      
        for(long sub:_c1.getR().keySet()){
            
            if(_r2.getR().containsValue(sub)){
               
                 Set<Long> keyall=  _c1.getR().get(sub);
               
                     for(long ka:keyall){
                        
                        SO sp= new  SO(sub,ka);
                        
                        
                        Set<Indexes> in=_c1.getrIndexUpdated().getKeys(sp);
                        _r4.getR().put(sub, ka);
                        for(Indexes kin:in){
                            
                            _r4.getrIndexUpdated().put(kin, sp);
                            
                              for(long val:_r2.getR().getKeys(sub)){
                                  
                                  for(long kin2:_r2.getrIndex().getKeys(new SO(val,sub))){
                                     // _pC.put(kin, kin2);
                                      
                                     
                                      _r4.getParentChild().put(kin2, kin.getGlobalIndex());
                                  }
                                  
                                
                       
                             }
                        }
                        

                          
                    }
                
                
                ///get the first key and get how many keys other table have, if there are more than 1
                
                _utils.checker=true;
    
 
                 
                    
                
                
            }
            
        }
     ///   this.eachJoinResult(_r4, _r3,_pC,_jpC);
        
      // graphPairResult(dd);
       
    }
    @Override
      public void ssHashJoinRC2(MultiBidirectionalIndex _c1,FinalView  _r2,MultiBidirectionalIndex  _r4, MultiBiMap<Indexes, Indexes> _pC){
        
        
        
        for(long key:_c1.getR().keySet()){
            
            if(_r2.getR().containsKey(key)){
             
                _utils.checker=true;
           
               
                
                Set<Long> c1val=_c1.getR().get(key); //put all in the new r whcich is r3
                for(long val:c1val){
                    
                    
                    
                    SO sp= new SO(key,val);
                        
                        
                        Set<Indexes> in=_c1.getrIndexUpdated().getKeys(sp);
                        
                        
                        _r4.getR().put(key, val);
                                
                    for(Indexes kin:in){
                        
                        _r4.getrIndexUpdated().put(kin, sp);
                        
                        for(long val2:_r2.getR().get(key)){
                            
                            ///get the index and use it for the insertion procedure
                            SO sp2= new SO(key,val2);
                            
                            
                            for(long kin2:_r2.getrIndex().getKeys(sp2)){
                                
                                _r4.getParentChild().put(kin2, kin.getGlobalIndex());
                               // _pC.put(kin, kin2);
                            }
                            
                            

                        }
                    }
                    
                     
                    
                     
                     
             
                     }
                
   
              
                
            
            //
        }
        
        
      
    }
    
    
    }  
    @Override
      public void ssHashJoinRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4) {

        for (long key : _c1.getR().keySet()) {

            if (_r2.getR().containsKey(key)) {

                Set<Long> r2val = _r2.getR().get(key); //put all in the new r whcich is r3

                Set<Long> c1val = _c1.getR().get(key);

              

                for (long val2 : c1val) {

                    SO sp = new SO(key, val2);
                    Set<Long> in = _c1.getrIndex().getKeys(sp);
                    for (long kin : in) {

                        for (long k2 : r2val) {
                            if (_r2.getrIndex().getKey(new SO(key, k2)) == kin) {
                                _utils.checker = true;
                                _r4.getR().put(key, val2);
                                _r4.getrIndex().put(kin, sp);
                            }
                        }
//                     

                    }
                }

            }
            //
        }

    }

    @Override
    public void osHashJoinRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4) {
       // dd.clear();
        //  jdd.clear();
        for (long sub : _c1.getR().keySet()) {

            if (_r2.getR().containsValue(sub)) {

                Set<Long> keyall = _r2.getR().getKeys(sub);

//                     for(long ka:keyall){
//                        
//                        SO sp= new SO(ka,sub);
//                        
//
////                        
//                        Set<Long> in =_r2.getrIndex().getKeys(sp);
//                        for(long kin: in){
//                           
//                            
//                             for(long val:_c1.getR().get(sub)){
//                            if(_c1.getrIndex().getKey(new SO(sub,val))==kin){
//                                _r3.getrIndex().put(kin, sp);
//                                _r3.getR().put(ka, sub);
//                            }
//                             }
//                            
//                        }
//                          
//                    }
                ///get the first key and get how many keys other table have, if there are more than 1
                for (long val : _c1.getR().get(sub)) {

                    SO sp = new SO(sub, val);
                    Set<Long> in = _c1.getrIndex().getKeys(sp);

                    for (long kin : in) {

                        for (long k2 : keyall) {
                            if (_r2.getrIndex().getKey(new SO(k2, sub)) == kin) {
                                _utils.checker = true;
                                _r4.getrIndex().put(kin, sp);
                                _r4.getR().put(sub, val);
                            }
                        }
//                        
//                        
//                        
//                        
//                   
//                    }
                    }

                }

            }

        }
    }

    @Override
    public void soHashJoinRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3) {

        for (long sub : _c1.getR().values()) {

            if (_r2.getR().containsKey(sub)) {

                ///first get the index value from r2
                //  long index=_r2.getrIndex().getKey(new SO(sub,_r2.getR().getValue(sub)));
                Set<Long> keyall = _r2.getR().get(sub);
                Set<Long> keyall2 = _c1.getR().getKeys(sub);
//                for(long val:keyall){
//                    
//                    SO sp= new SO(sub,val);   //may be make two objects of this kind
//                   Set<Long> in=_r2.getrIndex().getKeys(sp);
//                   for(long kin:in){
//                       
//                       
//                       for(long key:keyall2){
//                           if(_c1.getrIndex().getKey(new SO(key,sub))==kin){  ///change it later
//                               _r4.getrIndex().put(kin, sp);
//                               _r4.getR().put(sub, val);
//                           }
//                       }
//                   
//                       
//                    
//                   }
//                   
//                }

                for (long ka : keyall2) {    ///whats the point of this one????
                    SO sp = new SO(ka, sub);

                    Set<Long> in = _c1.getrIndex().getKeys(sp);

                    for (long kin : in) {

                        _r3.getrIndex().put(kin, sp);

                        for (long k2 : keyall) {    //change it later
                            if (_r2.getrIndex().getKey(new SO(sub, k2)) == kin) {
                               _utils. checker = true;
                                _r3.getrIndex().put(kin, sp);
                                _r3.getR().put(ka, sub);
                            }
                        }

                    }

                }
            }

        }
        // this.eachJoinResult(_r3, _r4);
    }

    @Override
    public void ooHashJoinRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3) {

        for (long obj : _c1.getR().values()) {
            if (_r2.getR().containsValue(obj)) {
              //  this.indexes.clear();

                ///get the keys from c1
                Set<Long> keyall = _c1.getR().getKeys(obj);

                Set<Long> keyall2 = _r2.getR().getKeys(obj);

                for (long keys : keyall) {

                    SO sp = new SO(keys, obj);
                    Set<Long> in = _c1.getrIndex().getKeys(sp);

                    for (long kin : in) {

                        for (long val : keyall2) { ///why fucking for stuff
                            if (_r2.getrIndex().getKey(new SO(val, obj)) == kin) {
                                _utils.checker = true;
                                _r3.getrIndex().put(kin, sp);
                                _r3.getR().put(keys, obj);
                            }
                        }

                    }

                }

                ///first get the index from the _r2
//                for(long keys:keyall2){
//                    
//               //   for(long kin:this.indexes)
//                      
//                   
//                   SO sp= new SO(keys,obj);
//                   
//                   Set<Long> in=_r2.getrIndex().getKeys(sp);
//                   
//                   for(long kin:in){
//                       
//                       for(long k2:keyall){   ///remove this loop
//                           
//                       
//                       
//                       
//                       if(_c1.getrIndex().getKey(new SO(k2,obj))==kin){
//                            _r4.getrIndex().put(kin,sp);
//                            _r4.getR().put(keys,obj);
//                       }
//                       }
//                   }
//                }
            }

        }

      // this.eachJoinResult(_r3, _r4);
    }
    @Override
 public void windowUpdate(long range, long step, FinalView[] _FV, long timestamp) {

        long tb =  1L;//(timestamp - range) / step;

        //tb = (long) Math.floor(tb);

        ///go through each view and trim the list and delete the rest of it
        for (int i = 0; i < _FV.length; i++) {
      
            removeMatchesFromFV(_FV[i].getTimeList(), tb, _FV[i]);
        }

        ///iterate over each entry in the FV to delete everythign less than timestamps
        ///remove everything in FV that is less than tb 
    }

    @Override
    public void removeMatchesFromFV(List<Long> timeList, long tb, FinalView _fv) {
        int index = getTheCuttingIndex(timeList, tb);
        if (index == -1 || index == 0) {
            return;
        } else if (index > 0) {
            ///found the element, remove  by index -1

            removalProcess(timeList, _fv, --index);
        } else if (index < 0) {
            /// remove til (-(index) -2)
            index = (-index - 1);
            removalProcess(timeList, _fv, index);

        }
    }

    @Override
    public void removalProcess(List<Long> timeList, FinalView _fv, int index) {

        for (int i = 0; i < index; i++) {  /// its not really optimised, may be used a queue instead of your shitty stuff
          //get the timestamp,
            ///get all the SO related to this timestamp
            for (SO sp : _fv.getTimeSPpairs().get(timeList.get(i))) {
                _fv.getR().remove(sp.getSub(), sp.getPred());
                
                _fv.getrIndex().removeValues(sp);
            }

          //  _fv.getTimeSPpairs().removeAll((long) timeList.get(i)); //change this HOw the FUCK???
            
          _fv.getTimeSPpairs().removeAll((long)timeList.get(i));
         
        }
        timeList.subList(0, index).clear();
            ///remove from the list as welll
       // timeList = timeList.subList(0, index);
    }

    @Override
    public int getTheCuttingIndex(List<Long> timeList, long tb) {

        
//        if(timeList.get(0) < tb){
//            return -1;
//        }
        
        return Collections.binarySearch(timeList, tb);

    }

}
