/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.qj.queryJoins;

import edu.cgpm.qj.interfaces.QueryProcInterface;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.jcwhatever.nucleus.collections.MultiBiMap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.datastructure.Indexes;
import edu.cgpm.datastructure.MultiBidirectionalIndex;
import edu.cgpm.datastructure.SO;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.AutomataState;
import edu.cgpm.rulesmodel.Dependability;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import org.semanticweb.yars.nx.Literal;

/**
 *
 * @author sydgillani
 */
public class QueryProcessor implements QueryProcInterface {
//    private int stop=0;
//    private  boolean checker=false;
//   // private boolean on2ndState=true;
//    private int stateID;
//    private final ArrayList<Long> indexes;
//    private final   List<Set<SO>> setList;
//   // private DictionaryOpImpl dic; ///delete it later
//    private int breakpt=0;
//    //  int test=0;
    //  public boolean resultHandler=false;
   private final IncQueryProcessor incQ;
//    private long indexCount;
    
    private final Utilities util;
    
    
   public  QueryProcessor(){
        this.util = new Utilities();
       incQ = new IncQueryProcessor();
     //  indexes= new ArrayList<>();
      // setList= new ArrayList<>();
      // indexCount=0;
   }
    
  
    @Override
    public void runFirstPhase(Collection<AutomataState> a, DictionaryOpImpl dic, int change, FinalView[] _FV, long timestamp,long range, long step){
     
        
        if(change == 1){
            // breakpt=10000000;
        // if(proceed==0){
     
            // stop=0;
        util.refresh();
        List<AutomataState> aStates= a.stream().filter(x->x.getRule().getC2().size()>0 ).collect(Collectors.toList());
            
            
            
            if(_FV[0].getR().isEmpty() && a.size()==aStates.size()){
                ////run the joins 
                ///if the joins produces the results then put it in FV as usual
                this.initialStage(aStates, dic, _FV, timestamp, range, step);
            }else if(!_FV[0].getR().isEmpty()){
                ////join all the triple patterns with c.size greater than zero
                ///if the all the joins produces the results then well done and if the size is equal to the automata size put it in FV
                ///else join only few of them produces the results, then take the one with R>0 and join them with FV
                //if the above list is equal to zero then send only the one with new C's to have a fun with FV
               // this.removeOldTriples(aStates, timestamp, range, step); ///remove the shity old triples first
                
                this.generalStage(a.size(), aStates, dic, _FV, timestamp, range, step);
            }
        }
    }
    
    
    
    @Override
    public void removeOldTriples(List<AutomataState> as, long time, long range, long step){
        
        List<AutomataState> _fas= as.stream().filter(x -> x.getRule().getC2().size() > 0 && x.getChange() == 1).collect(Collectors.toList());
        
         long tb =  (time - range) / step;
         
         tb = (long) Math.floor(tb);
         for(AutomataState rs:_fas){
             this.tripleRemoval(rs, tb);
         }
    }
    
    @Override
    public void tripleRemoval(AutomataState s, long tb){
        ////get all the timestamps from the list and sort it
        
       List<Long> tobeRemoved = s.getRule().getTimeSOpairs().keySet().stream().sorted().collect(Collectors.toList());
  if(tobeRemoved.get(0)<tb){
  tobeRemoved=tobeRemoved.stream().filter(x->x<tb).collect(Collectors.toList());
    
       if(!tobeRemoved.isEmpty()){
      Collection<SO> soTobeRemoved= s.getRule().getTimeSOpairs().removeAll(tobeRemoved);
       
        for (SO so:soTobeRemoved) {
            s.getRule().getC2().remove(so.getSub(), so.getPred());
        }
       }
}
    }
    
    
    
    @Override
    public void generalStage(int size,List<AutomataState> aStates, DictionaryOpImpl dic, FinalView[] _FV, long timestamp,long range, long step){
        
         refreshStructures(aStates); ///May be remove this for triple streams
         
         if(aStates.size()==1){
              incQ.run(aStates,_FV,dic,timestamp,range,step);
         }else{
         
         
         tpJoinAutomataStage(aStates); ///execute all the joins
         
        if(util.stop==0 && aStates.size() == size){///if all the joins have produced the results
         outputResultsAll(aStates, dic,_FV,timestamp,1);
        }else if(util.stop==0){
            ///simple send them to the incermenal matching stuff
             incQ.run(aStates,_FV,dic,timestamp,range,step);
        }else if(util.stop==1){
            ///Get all the new ones and send it for the joins between them,(should change it later), and send them for the incremental processing. A complex case though
            //Before send it for the joins you can also check it 
             util.stop=0;
            aStates = aStates.stream().filter(x->x.getChange()==1 ).collect(Collectors.toList());
             refreshStructures(aStates);
             if(!aStates.isEmpty() && aStates.size()> 1){
                 tpJoinAutomataStage(aStates);
                 
                 
                 if(util.stop==0){
                     incQ.run(aStates,_FV,dic,timestamp,range,step);
                 }
             }else{
                 incQ.run(aStates,_FV,dic,timestamp,range,step);
             }
        }
         }
    }
    
    
    @Override
    public void initialStage( List<AutomataState> aStates, DictionaryOpImpl dic,  FinalView[] _FV, long timestamp,long range, long step){
       ///Remove the earlier results
        
         refreshStructures(aStates);
        
         tpJoinAutomataStage(aStates); ///execute all the joins
         
        if(util.stop==0){///if all the joins have produced the results
         outputResultsAll(aStates, dic,_FV,timestamp,1);
        }
        
    }
   
    ////Something is not right over here
    @Override
    public void refreshStructures( List<AutomataState> aStates){
         for(AutomataState s:aStates){
               
            //  
             //   System.out.println(s.getStateId()+"    "+s.getRule().getC2().size());
                
               if(!s.getEdge().getRreal().getR().isEmpty()){
                        s.getEdge().getRreal().getR().clear();//.getR().clear();
                        s.getEdge().getRreal().getrIndex().clear();//rIndexClear();
                        s.getEdge().getRreal().getParentChild().clear();
                    }else if(!s.getEdge().getrCopied().getR().isEmpty()){
                        s.getEdge().getrCopied().getR().clear();//rClear();
                        s.getEdge().getrCopied().getrIndex().clear();//rIndexClear();
                        s.getEdge().getrCopied().getParentChild().clear();
                    }
               
               if(s.getRule().getDepends().size()>1){
                   s.getRule().getDepends().get(0).setJoined(0);
                   s.getRule().getDepends().get(1).setJoined(0);
               }else{
                   s.getRule().getDepends().get(0).setJoined(0);
               }
            }
    }
    
    
    

    @Override
     public void outputResultsAll(List<AutomataState> a,DictionaryOpImpl dic,FinalView[] _FV,long timestamp,int seq ){
           Set<Long> keys=null;
         util.setList.clear();
          // this.stateID=5;
        if(!a.get(util.stateID).getEdge().getRreal().getR().isEmpty()){
          
             keys=a.get(util.stateID).getEdge().getRreal().getrIndex().keySet();
             

        }else if(!a.get(util.stateID).getEdge().getrCopied().getR().isEmpty()){
           
             keys=a.get(util.stateID).getEdge().getrCopied().getrIndex().keySet();
        }
        
     
      
      for(long in:keys){
          // statesDFS(a.getStates().get(stateID), a, in);
         _resultExtraction(in, a,_FV,dic,timestamp,seq);
      //   printOut();
         seq++;
         util.setList.clear();
      }
      
      
        
     }
     
     
     
    @Override
     public void _resultExtraction(long index, List<AutomataState> a,FinalView[] _FV,DictionaryOpImpl dic,long timestamp,int seq){
        
         Set<SO> nSP;
       
            util.setList .clear();
             for(AutomataState s: a){
               nSP= new HashSet<>();
                     if(!s.getEdge().getRreal().getrIndex().isEmpty())
                {
                    
                         for (Iterator<Entry<Long, SO>> it = s.getEdge().getRreal().getrIndex().entries().iterator(); it.hasNext();) {
                             Entry<Long,SO> sp = it.next();
                             if(sp.getKey()==index){
                                 _FV[s.getStateId()].getR().put(sp.getValue().getSub(), sp.getValue().getPred());
                                 _FV[s.getStateId()].getTimeSPpairs().put(timestamp, sp.getValue());   ///temporal aspects like time tree should be captured over here
                               
                                   _FV[s.getStateId()].getrIndex().put(index, sp.getValue());
                                  
                                 s.getRule().getC2().remove(sp.getValue().getSub(), sp.getValue().getPred());
                                 s.getRule().getTimeSOpairs().remove(timestamp, new SO(sp.getValue().getSub(),sp.getValue().getPred()));
                                 nSP.add(sp.getValue());
                             }
                         }
                         //  _FV[s.getStateId()].getTimeIndexMap().put(timestamp, index);
                          util.setList.add(nSP);
                   
                    
                    
                } else{
                  
                     
                         for (Iterator<Entry<Long, SO>> it = s.getEdge().getrCopied().getrIndex().entries().iterator(); it.hasNext();) {
                             Entry<Long,SO> sp = it.next();
                             if(sp.getKey()==index){
                                 _FV[s.getStateId()].getR().put(sp.getValue().getSub(), sp.getValue().getPred());
                                 _FV[s.getStateId()].getTimeSPpairs().put(timestamp, sp.getValue());  ///again the tempoal aspects like time tree
                                 
                                    _FV[s.getStateId()].getrIndex().put(index, sp.getValue());
                                 
                                  s.getRule().getC2().remove(sp.getValue().getSub(), sp.getValue().getPred());
                                  
                                    s.getRule().getTimeSOpairs().remove(timestamp, new SO(sp.getValue().getSub(),sp.getValue().getPred()));
                                  nSP.add(sp.getValue());
                             }
                         }
                        // _FV[s.getStateId()].getTimeIndexMap().put(timestamp, index);
                         
                          util.setList.add(nSP);
                }
                     s.setChange(0);
                     if(seq==1)
                     _FV[s.getStateId()].getTimeList().add(timestamp);
                    
               }
               
               
      
      printOut(dic);
        
        
     }
     
     
    @Override
     public void printOut(DictionaryOpImpl dic){
         
      Set<List<SO>> result = Sets.cartesianProduct(util.setList);
                 result.forEach((outList) -> {
                     
                    outList .forEach((out) -> {
	 System.out.print("--> Subject  "+dic.getResourceIncremenal( out.getSub()) +"--> Object " + dic.getResourceIncremenal( out.getPred()));
                  
                 ///   System.out.println("--> "+" "+dic.getResourceIncremenal( out.getPred()) );
                    
                    Literal lg= dic.getLiteralIncremental(out.getPred());
                    if(lg != null)
                        System.out.print(lg.getValue().toString());
                   
                        System.out.println();
                  //  s.getRule().getC2().remove(out.getSub(), out.getPred());
});
                     System.out.println("######################################");
                     
                 });
             
        
     }
     

    
    
 
    /*
    public void testExtraction( Indexes index,Automata a){
        
         Set<SO> set = new HashSet<>();
       ///get it from  5 and 4 
        //use 4 to get from 3 and then 3 from 1, 1 from zweo and then zero from 2
          set = extractResults(a.getStates().get(2), a.getStates().get(1), index, set);
          System.out.println(set);
          this.setList.add(set);
        ////////////////////5-2 join//////////////
        
        ///now with 2 and 0/////
         set = extractResults(a.getStates().get(1), a.getStates().get(0), index, set);
          System.out.println(set);
        this.setList.add(set);
        ///////////////////////////////////////////////
        
        /////0 and 1/////
           
   set = new HashSet<>();
            set = extractResults(a.getStates().get(5), a.getStates().get(4), index, set);
          System.out.println(set);
        this.setList.add(set); 
        /////////////////////////////////////////////////
        
        ///now 1 and 3////
        
         set = extractResults(a.getStates().get(4), a.getStates().get(3), index, set);
          System.out.println(set);
        this.setList.add(set); 
        
        ///////////////////////////////////////////////////////////
       
    
             
    }
    
    */
     /*
    public Set<SO> extractResults(AutomataState s, AutomataState js, Indexes index, Set<SO> set){
   Set<SO> newSet = new HashSet<>();
        if(set.isEmpty()){
            
            
            
            ///get it from S and 
            
            if(!s.getEdge().getRreal().getR().isEmpty()){
                
                
              //  for (Indexes kin:index){   ////change of for loop, put it in there
                    ///gor around s  thats it
                    
                    
                         for (Iterator<Entry<Indexes, SO>> it = s.getEdge().getRreal().getrIndexUpdated().entries().iterator(); it.hasNext();) {
                             Entry<Indexes,SO> sp = it.next();
                             if(sp.getKey().getGlobalIndex()==index.getGlobalIndex()){
                                 
                                set.add(sp.getValue());
                             }
                         }
                         
                    
                    
               // }
                  setList.add(set);
                  System.out.println(set);
                secondStage(s, js, newSet, set);
                
                
                
            }else if(!s.getEdge().getrCopied().getR().isEmpty()){
                
                    ///gor around s  thats it
                    
                    
                         for (Iterator<Entry<Indexes, SO>> it = s.getEdge().getrCopied().getrIndexUpdated().entries().iterator(); it.hasNext();) {
                             Entry<Indexes,SO> sp = it.next();
                             if(sp.getKey().getGlobalIndex()==index.getGlobalIndex()){
                                 
                                set.add(sp.getValue());
                             }
                         }
                        setList.add(set);
                            System.out.println(set);
                    secondStage(s, js, newSet, set);
                   // return set;
                    
               
            }
           
     }else{
         ///go around the set and get the childeren from the results set and then get the values from join state
         
          secondStage(s, js, newSet, set);
     }
        
      return newSet;  
    }
    */
  /*   public void secondStage(AutomataState s, AutomataState js,Set<SO> newSet, Set<SO> set){
         
        // Set<SP> toBeRemoved= new HashSet<>();
         for(SO sp: set){
             /////get the parent from s
             
             
             Set<Indexes> kin=null;
             if(!s.getEdge().getRreal().getR().isEmpty()){
                 kin = s.getEdge().getRreal().getrIndexUpdated().getKeys(sp);
             }else if (!s.getEdge().getrCopied().getR().isEmpty()){
                 kin = s.getEdge().getrCopied().getrIndexUpdated().getKeys(sp);
             }
             
             
             for(Indexes k :kin){
                 ///get from js
                 //get the parent from
                 
                 
                 
                 for(Indexes d:  s.getParentChild().get(k)){
                     
                     if(!js.getEdge().getRreal().getR().isEmpty()){
                         newSet.addAll(js.getEdge().getRreal().getrIndexUpdated().get(d)); ///if not then remove it
                         ///from the other set as well
                     }else if (!js.getEdge().getrCopied().getR().isEmpty()){
                         
                         if(js.getEdge().getrCopied().getrIndexUpdated().get(d).isEmpty()){
                             ///remove the stuff from here, how to remove at the same time
                            // set.remove(sp); if its not there then remove from the index table as well
                            
                           //  toBeRemoved.add(sp);
                         }else{
                             newSet.addAll(js.getEdge().getrCopied().getrIndexUpdated().get(d));
                         }
                         
                         
                         
                     }
                     
                 }
                 
             }
             
             
             
         }
        /// set.removeAll(toBeRemoved);
        
     }
*/
    
    @Override
        public void tpJoinAutomataStage(List<AutomataState> automata){
        
        
         
            for(AutomataState s:automata){
                tripleJoinStage(s,automata);
            }
        
        
    }
        
    @Override
        public void tripleJoinStage(AutomataState currState, List<AutomataState>  a){
        
        
        
        
        for(Dependability dd:currState.getRule().getDepends()){  ///not sure about the following if block
            if(dd.getJoined()==0  && (!currState.getRule().getC2().isEmpty() ||!currState.getEdge().getRreal().getR().isEmpty() || !currState.getEdge().getrCopied().getR().isEmpty())){
                ///find the join state
                AutomataState sJoin = a.stream().filter(x->x.getStateId()==dd.getDependabilty_id()).findFirst().orElse(null);
                if(sJoin==null){
                    continue;
                }
                ////if sJoin is null then continue;
           //     System.out.println(" #################  ");
             //   System.out.println("Current State "+ currState.getRule().getPredicate().getPredicateValue().toString() + " " +currState.getRule().getSubject().getProjection());
               
                
               // System.out.println("Join State "+ sJoin.getRule().getPredicate().getPredicateValue().toString() + " " + sJoin.getRule().getSubject().getProjection());
                 /////////////////
                
                
           Dependability jdd =null;
                
           
           //dd.setOrderJoin(sJoin.getStateId());
          // jdd.setOrderJoin(currState.getStateId()); ///whts the fucking poin of this
           
         ///do stuff with join dd and dd
           
           ///from to 
              
                
                //Remove the stop ==0, as we need tp join as many as possible
                if(dd.getDependability_On()==1 && dd.getDependability_part()==1 ){ ///stop==0;///before send it over here check if the join state is not empty/// Qiute a 
                    ///shit case but its for the triple streams
                    
                    /// incrementalSSJoinStage1(sJoin,currState,a);
                   jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==1 && x.getDependability_part()==1).findFirst().orElse(null);
                    incrementalSSJoinStage(currState,sJoin,dd,jdd);
                    
                }else if(dd.getDependability_On()==1 && dd.getDependability_part()==0){
                    ///s for current stae
                    //  incrementalOSJoin(currState,sJoin,a);
                      jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==0 && x.getDependability_part()==1).findFirst().orElse(null);
                   incrementalOSJoinStage(currState, sJoin,dd,jdd);
                    
                }else if(dd.getDependability_On()==0 && dd.getDependability_part()==1 ){
                    //  incrementalSOJoin(currState,sJoin,a);
                      jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==1 && x.getDependability_part()==0).findFirst().orElse(null);
                   incrementalSOJoinStage(currState, sJoin,dd,jdd);
                }else if(dd.getDependability_On()==0 && dd.getDependability_part()==0 ){
                    
                     jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==0 && x.getDependability_part()==0).findFirst().orElse(null);
                    incrementalOOJoinStage(currState,sJoin,dd,jdd);
                }
                
                
          //    System.out.println("###############Current State ##################");
           //    outputIndex(currState);
              //  outputMaterialsedview(currState);
           //   System.out.println("#########Join State ##################");
           //     outputIndex(sJoin);
              //  outputMaterialsedview(sJoin);
                    if(!currState.getEdge().getRreal().getR().isEmpty()){
                    ///then get 
                    
                    if(currState.getEdge().getRreal().getrIndexUpdated().size()<= util.breakpt){
                        util.breakpt=currState.getEdge().getRreal().getrIndexUpdated().size();
                        util.stateID=currState.getStateId();
                    }
                    
                    
                    
                }else if (!currState.getEdge().getrCopied().getR().isEmpty()){
                    if(currState.getEdge().getrCopied().getrIndexUpdated().size()<= util.breakpt){
                   util.breakpt=currState.getEdge().getrCopied().getrIndexUpdated().size();
                    util.stateID=currState.getStateId();
                    }
                }
                
                
                
                
                
                if(!sJoin.getEdge().getRreal().getR().isEmpty()){
                    ///then get 
                    
                    if(sJoin.getEdge().getRreal().getrIndexUpdated().size()<= util.breakpt){
                      util.breakpt=sJoin.getEdge().getRreal().getrIndexUpdated().size();
                       util.stateID=sJoin.getStateId();
                    }
                    
                    
                    
                }else if (!sJoin.getEdge().getrCopied().getR().isEmpty()){
                    if(sJoin.getEdge().getrCopied().getrIndexUpdated().size()<=util. breakpt){
                       util.breakpt=sJoin.getEdge().getrCopied().getrIndexUpdated().size();
                       util.stateID=sJoin.getStateId();
                    }
                }
                 System.out.println("Stop " +util.stop);
//                if(stop==0){
//                    currState.setChange(0);
//                    sJoin.setChange(0);
//                }
                
                
                
            }
            
            
           
        }
        
        
        
    }
       
    
  
    @Override
    public void incrementalSSJoinStage(AutomataState currState,AutomataState joinState,Dependability dd,Dependability jdd){
        
        
        ///if the join state r and r join are empty
        
      
        
        if(joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty()){ /// the use the both C and put it in Rreal of both
            
            ///first check if the r is empty or ecopied is emptied for the currstate
            
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                   // long index=util.indexCount;
                
                if(currState.getRule().getC2().keySize()<joinState.getRule().getC2().keySize()){
                    
                    
                    
                    ///Send the index over here
                  
                    // Change
                 ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                } else{
                    
                    
                    ///Change
                ssJoinOptimisedForCC(joinState.getRule().getC2(),currState.getRule().getC2(),  joinState.getEdge().getRreal(), currState.getEdge().getRreal());
                }
                
                
            }else
                
                
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    
                    
                    this.ssJoinOptimisedForRC(joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied());
                    
                    
                    
                    
                    
                    if(util.checker){
                        
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    this.ssJoinOptimisedForRC(joinState.getRule().getC2(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal());
                    
                    
                    if(util.checker){
                        
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                    
                    
                }
            
            
            //   this.ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
            
            
            
            
        }else if(!joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty() ){
            //use Real
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                
                this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), currState.getEdge().getRreal(),joinState.getEdge().getrCopied() );
                
                
                if(util.checker){
                    
                    joinState.getEdge().getRreal().getR().clear();
                    joinState.getEdge().getRreal().getrIndex().clear();
                }
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    if(joinState.getEdge().getRreal().getR().keySize()<currState.getEdge().getRreal().getR().keySize()){
                        
                        this.ssJoinOptimisedForRC2(joinState.getEdge().getRreal(), currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getEdge().getrCopied());
                    }else{
                        this.ssJoinOptimisedForRC2(currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getEdge().getrCopied(), joinState.getEdge().getrCopied());
                    }
                    
                    
                    if(util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndex().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(joinState.getEdge().getRreal().getR().keySize()<currState.getEdge().getrCopied().getR().keySize()){
                        this.ssJoinOptimisedForRC2(joinState.getEdge().getRreal(), currState.getEdge().getrCopied(), joinState.getEdge().getrCopied(),currState.getEdge().getRreal());
                    }else{
                        this.ssJoinOptimisedForRC2(currState.getEdge().getrCopied(),joinState.getEdge().getRreal(), currState.getEdge().getRreal(),joinState.getEdge().getrCopied());
                    }
                    
                    if(util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndex().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                }
            
            
            
            
            // this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal());
            
            //    if(checker){
            //      joinState.getEdge().getRreal().getR().clear();
            //    joinState.getEdge().getRreal().getrIndex().clear();
            // }
            
        }else if(joinState.getEdge().getRreal().getR().isEmpty() && !joinState.getEdge().getrCopied().getR().isEmpty()){
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                
                if(util.checker){
                   
                    joinState.getEdge().getrCopied().getR().clear();
                    joinState.getEdge().getrCopied().getrIndex().clear();
                    
                }
                
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    if(joinState.getEdge().getrCopied().getR().keySize()< currState.getEdge().getRreal().getR().keySize()) {
                        this.ssJoinOptimisedForRC2(joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied());
                    }else{
                        this.ssJoinOptimisedForRC2(currState.getEdge().getRreal(),joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(),joinState.getEdge().getRreal());
                    }
                    if(util.checker){
                       
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndex().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                    
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    
                    if(joinState.getEdge().getrCopied().getR().keySize()<currState.getEdge().getrCopied().getR().keySize()){
                        this.ssJoinOptimisedForRC2(joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal());
                    }else{
                        this.ssJoinOptimisedForRC2(currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                    }
                    
                    
                    if(util.checker){
                        
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndex().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                    
                }
            
            
            
            
            
            //      this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(), joinState.getEdge().getRreal(), currState.getEdge().getRreal());
            
            
            
            
        }
        
        
        
        
        
        
        
        //// check the tablesomappedcopied
        
        //TODO: remove the stop from the system
        ////////////////
        if(util.checker)
        {
            dd.setJoined(1);
             if(jdd!=null)
           jdd.setJoined(1);
           /// currState.setJoined(1);
           // joinState.setJoined(1);
            
          //  currState.setChange(0);
            //  automata.getStates().get(id).getRule().getTableSOMapped().removeAll(  automata.getStates().get(id).getEdge().getTableSOMapped()); //change it
            //   removeFromC(currState.getRule().getC(),currState.getEdge().getR());
           util. checker=false;
            
        }else{
            util.stop=1;
        }
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
  ///Edit 1
    @Override
    public void ssJoinOptimisedForCC( MultiBiMap<Long, Long> _c1,MultiBiMap<Long, Long> _c2,MultiBidirectionalIndex _r1,MultiBidirectionalIndex  _r2){
        
        ///Take the index from the top
        ///Increment the index
       
        for(long key:_c1.keySet()){
            
            if(_c2.containsKey(key)){
                util.checker=true;
                util.indexes.clear(); ///dont do it every fucking time
               
                Set<Long> keyall=_c1.get(key);
                for(long val:keyall){
                     _r1.getR().put(key, val);
                     
                    
                    _r1.getrIndex().put(++util.indexCount, new SO(key,val));
                
                  util.indexes.add(util.indexCount);///delete it 
                   
                }
                
              
                
                Set<Long> r2val=_c2.get(key);
                for(long val:r2val){
                    
                      _r2.getR().put(key, val);
                     
                   
                         for(long kin:util.indexes){
                             _r2.getrIndex().put(kin, new SO(key,val));
                         }
                      
                     
                }                
            }
            //
        }
        
        
      //this.eachJoinResult(_r1, _r2,_pC,_jpC);
    //  graphPairResult(dd);
    }
    
    
    ///Edit 2
    
    /// chagen the multi BiMap tp multimap
   
    @Override
    public void ssJoinOptimisedForRC(MultiBiMap<Long, Long> _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex  _r4,MultiBidirectionalIndex _r3){
        
       // System.out.println("C1 "+ _c1.keySize());
       // System.out.println("R2 "+  _r2.getR().keySize());
      //  if(_c1.keySize()<_r2.getR().keySize()){
            
           //_C1 is a join state
         //   dd.clear();
            for(long key:_c1.keySet()){
                
                if(_r2.getR().containsKey(key)){
                     util.indexes.clear();
                     //  this.parentChild.clear();
                    ///first get the index value from _r
                    util.checker=true;
                   
                    
                    
                    Set<Long> r2val=_r2.getR().get(key); //put all in the new r whcich is r3
                    for(long val:r2val){
                        _r3.getR().put(key,val);
                        Set<Long> in = _r2.getrIndex().getKeys(new SO(key, val));
              
                        for(long kin:in){
                            
                            
                             _r3.getrIndex().put(kin, new SO(key,val));
                            util.indexes.add(kin);
                         }
                        
                       
                    }
                    
                    
                    ///now put the stuff in r3 which is for the join state
                    
                  
                    
                    Set<Long> c1val=_c1.get(key);
                    for(long val2:c1val){
                        
                        _r4.getR().put(key,val2);
                        for(long in:util.indexes){
                            
                            _r4.getrIndex().put(in, new SO(key,val2));
                            
                        }
//
                        
                    }
                    
                    
                }
                //
            }
            
//        }else{
//            
//            
//            
//            for(long key:_r2.getR().keySet()){
//                
//                if(_c1.containsKey(key)){
//                    this.indexes.clear();
//                    
//                    this.parentChild.clear();
//                    ///first get the index value from _r
//                    checker=true;
//                    // _r2.getR().getValue(key);
//                    
//                  //  long index =0; //_r2.getrIndex().getKey(new SO(key, _r2.getR().getValue(key)));
//                    
//                    
//                    
//                    
//                    Set<Long> r2val=_r2.getR().get(key); //put all in the new r whcich is r3
//                    
//                    ///get the value from _r2.getindex
//                    for(long val:r2val)
//                        
//                    {   //index = _r2.getrIndex().getKey(new SO(key, val));///get keys
//                        _r3.getR().put(key,val);
////                        Set<Long> in2=_r2.getrIndex().getKeys(new SO(key, val));
///////get all index vales
////                        for(long kin:in2) 
////                        {
////                            this.indexes.add(kin);
////                            
////                            
////                            _r3.getrIndex().put(kin, new SO(key,val));
////                        }
//                           Set<Indexes> ind=_r2.getrIndexUpdated().getKeys(new SO(key, val));
//                         
//                        for(Indexes kin:ind){
//                           // indexes.add(kin.getGlobalIndex());
//                            this.parentChild.add(kin);
//                          _r3.getrIndexUpdated().put(kin, new SO(key,val));   
//                        }
//                    }
//                    
//                    
//                    ///now put the stuff in r3 which is for the join state
//                    
//                    
//                    
//                    Set<Long> c1val=_c1.get(key);
//                    for(long val2:c1val){
//                        _r4.getR().put(key,val2);
//                        for(Indexes in:this.parentChild){
//                        
//                            
//                            Indexes ind= new Indexes(++perTableIndex,in.getGlobalIndex());
//                          //   dd.put(in, ++indexCount);
//                            //  dd.put(indexCount,in);/// remove this later
//                            _r4.getrIndexUpdated().put(ind, new SO(key,val2));
//                            _pC.put(in, ind);
//                            _jpC.put(ind, in);
//                            //_r4.getrIndex().put(in, new SO(key,val2));
//                        }
//                    }
//                }
//                //
//            }
//            
//        }
//        
        
        
        
        ///clear every thing in the R2
        //    _r2.getR().clear();
        //  _r2.getrIndex().clear();
        
   //   this.eachJoinResult(_r4, _r3,_pC,_jpC);
    
    //graphPairResult(dd);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    @Override
    public void ssJoinOptimisedForRC2(MultiBidirectionalIndex _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex  _r4,MultiBidirectionalIndex _r3){
        
        
        
        for(long key:_c1.getR().keySet()){
            
            if(_r2.getR().containsKey(key)){
              
             
               
                
               
                
                Set<Long> r2val=_r2.getR().get(key); //put all in the new r whcich is r3
                   
                Set<Long> c1val=_c1.getR().get(key);
                for(long val:r2val){
                    
                    SO sp=new SO(key,val);
                    
                     Set<Long> in2=_r2.getrIndex().getKeys(sp);
                     
                     for(long kin:in2){
                         
                         
                         for(long vals:c1val){
                             
                             
                             SO sp2=new SO(key,vals);
                             for (long krr:_c1.getrIndex().getKeys(sp2)) {
                                if(krr==kin){
                                    
                                       util.checker=true;
                                    _r3.getrIndex().put(kin, sp); 
                                     _r3.getR().put(key,val);
                                     
                                     _r4.getR().put(key,vals);
                                    _r4.getrIndex().put(krr,sp2);
                               } 
                             }
                               
                         }
                         
                       
                              
                     }
                     
             
                     }
                
                
                
                
               
             
//                for(long val2:c1val){
//                    
//                     
//                     SO sp= new SO(key,val2);
//                     Set<Long> in=_c1.getrIndex().getKeys(sp);
//                 for(long kin:in){
//                     
//                     for(long k2:r2val){
//                          if(_r2.getrIndex().getKey(new SO(key,k2))==kin){
//                              _r4.getR().put(key,val2);
//                               _r4.getrIndex().put(kin, sp);
//                          }
//                     }
//                     
//                     
//                     
//                   
//                 }                 
//                }
                
            }
            //
        }
        
        
      
    }
    
    
    
    
    
    
    
    
   
    
    
    
    
    
    ///////////////////////######################################################################////////////////////////////////////////
    
   
    @Override
    public void incrementalSOJoinStage(AutomataState currState, AutomataState joinState,Dependability dd,Dependability jdd){
        
        if(joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty()){ /// the use the both C and put it in Rreal of both
            
            ///first check if the r is empty or ecopied is emptied for the currstate
            
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                //  this.ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
              //  long index=this.indexCount;
                
                if(currState.getRule().getC2().size()<joinState.getRule().getC2().size()){
                    
                    
                    this.soHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                }else{
                    this.osHashJoinCC( joinState.getRule().getC2(),currState.getRule().getC2(), joinState.getEdge().getRreal(),currState.getEdge().getRreal());
                }
                
                
            }else
                
                
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    ///reverse function
                    
                    this.osHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild(),2);
                    if( util.checker){
                    
                        
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    
                    
                    //reverse function
                    this.osHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild(),2);
                    
                    
                    if( util.checker){
                       
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                    
                    
                }
            
            
            //   this.ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
            
            
            
            
        }else if(!joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty() ){
            //use Real
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.soHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                
                
                if( util.checker){
                   
                    joinState.getEdge().getRreal().getR().clear();
                    joinState.getEdge().getRreal().getrIndex().clear();
                }
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    if(currState.getEdge().getRreal().getR().size()<joinState.getEdge().getRreal().getR().size()){
                        
                        soHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getRreal(), currState.getEdge().getrCopied(),joinState.getEdge().getrCopied());
                    }else{
                        osHashJoinRC2(joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    if( util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndex().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(currState.getEdge().getrCopied().getR().size()<joinState.getEdge().getRreal().getR().size()){
                        soHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getRreal(), currState.getEdge().getRreal(),joinState.getEdge().getrCopied());
                    }else{
                        osHashJoinRC2(joinState.getEdge().getRreal(),currState.getEdge().getrCopied(), joinState.getEdge().getrCopied(),currState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    if( util.checker){
                        
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndex().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                }
            
            
            
            
            // this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal());
            
            //    if(checker){
            //      joinState.getEdge().getRreal().getR().clear();
            //    joinState.getEdge().getRreal().getrIndex().clear();
            // }
            
        }else if(joinState.getEdge().getRreal().getR().isEmpty() && !joinState.getEdge().getrCopied().getR().isEmpty()){
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.soHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                
                if( util.checker){
                    
                    joinState.getEdge().getrCopied().getR().clear();
                    joinState.getEdge().getrCopied().getrIndex().clear();
                    
                }
                
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    if(currState.getEdge().getRreal().getR().size()<joinState.getEdge().getrCopied().getR().size()){
                        soHashJoinRC2(currState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(),joinState.getEdge().getRreal());
                    }else{
                        osHashJoinRC2(joinState.getEdge().getrCopied(),currState.getEdge().getRreal(),joinState.getEdge().getRreal(), currState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    
                    if( util.checker){
                       
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndex().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                    
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(currState.getEdge().getrCopied().getR().size()<joinState.getEdge().getrCopied().getR().size()){
                        soHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(), currState.getEdge().getRreal(),joinState.getEdge().getRreal());
                    }else{
                        osHashJoinRC2(joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(), currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    
                    
                    
                    if( util.checker){
                       
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndex().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                    
                }
            
            
            
            
            
        }
        
        //TODO: MOVE this shit somewhere or delete the fuck out it
        if( util.checker){
            dd.setJoined(1);
             if(jdd!=null)
           jdd.setJoined(1);
             
             
             util.checker=false;
            
        }else{
             util.stop=1;
        }
    }
    
    ///////////////////////######################################################################////////////////////////////////////////
  
    @Override
    public void incrementalOSJoinStage(AutomataState currState, AutomataState joinState,Dependability dd,Dependability jdd){//AutomataState currState, AutomataState joinState, Automata a, long pred1, long pred2){
        
      
        
        
        if(joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty()){ /// the use the both C and put it in Rreal of both
            
            ///first check if the r is empty or ecopied is emptied for the currstate
             
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                //  this.ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                // long index =this.indexCount;
                if(currState.getRule().getC2().size()<joinState.getRule().getC2().size()){
                    
                    ///send it over here
                    
                    this.osHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                }else{
                    
                    
                    //send it over here
                    this.soHashJoinCC(joinState.getRule().getC2(),currState.getRule().getC2(),joinState.getEdge().getRreal(), currState.getEdge().getRreal());
                }
                
                
            }else
                
                
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    // this.osHashJoinRC(null, null, null, null);
                    
                    
                    
                    this.soHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    
                    
                    if( util.checker){
                       
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    
                    this.soHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    
                    
                    if( util.checker){
                       
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                    
                    
                }
            
            
            //   this.ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
            
            
            
            
        }else if(!joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty() ){
            //use Real
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.osHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild(),1);
                
                
                if( util.checker){
                   
                    joinState.getEdge().getRreal().getR().clear();
                    joinState.getEdge().getRreal().getrIndex().clear();
                }
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    
                    if(currState.getEdge().getRreal().getR().size()<joinState.getEdge().getRreal().getR().size()){
                        osHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        soHashJoinRC2(joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getEdge().getrCopied(),currState.getEdge().getrCopied());
                    }
                    
                    
                    
                    
                    if( util.checker){
                        
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndex().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(currState.getEdge().getrCopied().getR().size()<joinState.getEdge().getRreal().getR().size()){
                        osHashJoinRC2( currState.getEdge().getrCopied(),joinState.getEdge().getRreal(),currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        soHashJoinRC2( joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(),currState.getEdge().getRreal());
                    }
                    
                    
                    
                    if( util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndex().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                }
            
            
            
            
            // this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal());
            
            //    if(checker){
            //      joinState.getEdge().getRreal().getR().clear();
            //    joinState.getEdge().getRreal().getrIndex().clear();
            // }
            
        }else if(joinState.getEdge().getRreal().getR().isEmpty() && !joinState.getEdge().getrCopied().getR().isEmpty()){
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.osHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(),  currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild(),1);
                
                if( util.checker){
                  
                    joinState.getEdge().getrCopied().getR().clear();
                    joinState.getEdge().getrCopied().getrIndex().clear();
                    
                }
                
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    
                    if(currState.getEdge().getRreal().getR().size()<joinState.getEdge().getrCopied().getR().size()){
                        osHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getrCopied() , currState.getEdge().getrCopied(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        soHashJoinRC2(joinState.getEdge().getrCopied() ,currState.getEdge().getRreal(), joinState.getEdge().getRreal(), currState.getEdge().getrCopied());
                    }
                    //   osHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getrCopied() , currState.getEdge().getrCopied(),joinState.getEdge().getRreal());
                    
                    if( util.checker){
                      
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndex().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                    
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(currState.getEdge().getrCopied().getR().size()<joinState.getEdge().getrCopied().getR().size()){
                        osHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(), currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        soHashJoinRC2(joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal());
                    }
                    
                    
                    
                    
                    
                    if( util.checker){
                        
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndex().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                    
                }
            
            
            
            
            
        }
        if( util.checker){
            dd.setJoined(1);
             if(jdd!=null)
            jdd.setJoined(1);
             
             
             util.checker=false;
            
        }else{
             util.stop=1;
        }
    }
    
    
    ///////////////////////######################################################################////////////////////////////////////////
    
    
    
   
    @Override
    public void incrementalOOJoinStage(AutomataState currState, AutomataState joinState,Dependability dd,Dependability jdd){
        
      
        if(joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty()){ /// the use the both C and put it in Rreal of both
            
            ///first check if the r is empty or ecopied is emptied for the currstate
            
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                //  this.ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                  //long index =this.indexCount;
                if(currState.getRule().getC2().valueSize()<joinState.getRule().getC2().valueSize()){
                    
                    ///send it over here
                    
                    this.ooHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                }else{
                    
                    //send it over here
                    this.ooHashJoinCC(joinState.getRule().getC2(),currState.getRule().getC2(), joinState.getEdge().getRreal(),currState.getEdge().getRreal());
                }
                
                // this.ooHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                
                
                
            }else
                
                
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    // this.osHashJoinRC(null, null, null, null);
                    this.ooHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied());
                    if( util.checker){
                        
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    this.ooHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal());
                    
                    
                    if( util.checker){
                        
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                    
                    
                }
            
            
            //   this.ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
            
            
            
            
        }else if(!joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty() ){
            //use Real
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.ooHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(), joinState.getEdge().getrCopied());
                
                
                if( util.checker){
                    joinState.getEdge().getRreal().getR().clear();
                    joinState.getEdge().getRreal().getrIndex().clear();
                }
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    if(joinState.getEdge().getRreal().getR().valueSize()<currState.getEdge().getRreal().getR().valueSize()){
                        ooHashJoinRC2(joinState.getEdge().getRreal(), currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getEdge().getrCopied());
                    }else{
                        ooHashJoinRC2( currState.getEdge().getRreal(),joinState.getEdge().getRreal(), currState.getEdge().getrCopied(),joinState.getEdge().getrCopied());
                    }
                    
                    if( util.checker){
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndex().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(joinState.getEdge().getRreal().getR().valueSize()<currState.getEdge().getrCopied().getR().valueSize()){
                        ooHashJoinRC2(joinState.getEdge().getRreal(), currState.getEdge().getrCopied(), joinState.getEdge().getrCopied(),currState.getEdge().getRreal());
                    }else{
                        ooHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getRreal(),currState.getEdge().getRreal(), joinState.getEdge().getrCopied());
                    }
                    
                    if( util.checker){
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndex().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                }
            
            
            
            
            // this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal());
            
            //    if(checker){
            //      joinState.getEdge().getRreal().getR().clear();
            //    joinState.getEdge().getRreal().getrIndex().clear();
            // }
            
        }else if(joinState.getEdge().getRreal().getR().isEmpty() && !joinState.getEdge().getrCopied().getR().isEmpty()){
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.ooHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(),  currState.getEdge().getRreal(),joinState.getEdge().getRreal());
                
                if( util.checker){
                    joinState.getEdge().getrCopied().getR().clear();
                    joinState.getEdge().getrCopied().getrIndex().clear();
                    
                }
                
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    
                    if(joinState.getEdge().getrCopied().getR().valueSize()<currState.getEdge().getRreal().getR().valueSize()){
                        ooHashJoinRC2(joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied());
                    }else{
                        ooHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(), joinState.getEdge().getRreal());
                    }
                    
                    
                    if( util.checker){
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndex().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndex().clear();
                        
                        
                    }
                    
                    
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    
                    if(joinState.getEdge().getrCopied().getR().valueSize()<currState.getEdge().getrCopied().getR().valueSize()){
                        ooHashJoinRC2(joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal());
                    }else{
                        ooHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(),currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                    }
                    
                    
                    if( util.checker){
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndex().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndex().clear();
                        
                        
                    }
                    
                    
                }
            
            
            
            
            
        }
        if( util.checker){
             util.checker=false;
             dd.setJoined(1);
             if(jdd!=null)
            jdd.setJoined(1);
        }else{
            util. stop=1;
        }
        
        
    }
    
    
    
    
    
    
    
    ///////////////////////######################################################################////////////////////////////////////////
    ///Modify it
   
    @Override
    public void osHashJoinCC(MultiBiMap<Long, Long> _c1,MultiBiMap<Long, Long> _c2,MultiBidirectionalIndex _r1,MultiBidirectionalIndex  _r2){
        
        for(Long o:_c1.keySet()){//change the order
            
            if(_c2.containsValue(o)){
                util. checker=true;
                 util.indexes.clear();
               
                Set<Long> val=_c1.get(o); ///why this
                for(long v:val){
                    
                    _r1.getR().put(o, v);
                    _r1.getrIndex().put(++ util.indexCount,new SO(o, v));
                     util.indexes.add( util.indexCount);
                   
                }
             //   System.out.println("Values: "+ _c2.getKeys(index));
                ////////////////////
                Set<Long> keys =_c2.getKeys(o);  ///change it if its too expensive
                for(long k:keys){
                     _r2.getR().put(k, o);   //TODO: should I get all the values
                     
                     for(long kin: util.indexes)
                _r2.getrIndex().put(kin,new SO(k, o));  ///change it later
                }
                
             
            }
            
        }
        
        
        
   //   this.eachJoinResult(_r1, _r2,_pC,_jpC);
        
        
    }
    
    
   //Edit 4  ..order shit
    @Override
    public void osHashJoinRC(MultiBiMap<Long, Long> _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex  _r4,MultiBidirectionalIndex _r3,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC, int order){
        
        
      //  if(_c1.keySize()<_r2.getR().valueSize()){
            
            for(long sub:_c1.keySet()){
                
                if(_r2.getR().containsValue(sub)){
                    ///first get the index value from r2
                  
                    util.indexes.clear();
                   // this.parentChild.clear();
                    Set<Long> keyall=_r2.getR().getKeys(sub);
                    
                    for(long ka:keyall){
                        _r3.getR().put(ka, sub);
                         SO sp= new SO(ka, sub);
                        // index=_r2.getrIndex().getKey(sp);
                         
                         Set<Long> in= _r2.getrIndex().getKeys(sp);
                         for(long kin:in){
                              util.indexes.add(kin);
                         _r3.getrIndex().put(kin, sp);
                         }
//                         
                         
                         
                         
                
                    }
                    
                     util.checker=true;
                    
               
                    
                    for(long val:_c1.get(sub)){
                        ///each of the value should be included in it with all the indexes
                        _r4.getR().put(sub, val);
                        
                        for(long in: util.indexes){
                            
                         
                            
                            
                             
                           _r4.getrIndex().put(in, new SO(sub,val));
                        }
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                }
            }
                
  
        
        
   this.eachJoinResult(_r4, _r3,_jpC,_pC);   
    // graphPairResult(dd);   
    }
    
    
    ///Edit 6
    
    @Override
    public void osHashJoinRC2(MultiBidirectionalIndex _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex  _r4,MultiBidirectionalIndex _r3,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
       // dd.clear();
      //  jdd.clear();
        for(long sub:_c1.getR().keySet()){
            
            if(_r2.getR().containsValue(sub)){
             
                 Set<Long> keyall=  _r2.getR().getKeys(sub);
               
                     for(long ka:keyall){
                        
                        SO sp= new SO(ka,sub);
                        

//                        
                        Set<Long> in =_r2.getrIndex().getKeys(sp);
                        for(long kin: in){
                           
                           
                             for(long val:_c1.getR().get(sub)){
                                 
                                 SO sp2= new SO(sub,val);
                                 
                                 for (long krr:_c1.getrIndex().getKeys(sp2)) {
                                     if(krr==kin){
                                _r3.getrIndex().put(kin, sp);
                                _r3.getR().put(ka, sub);
                                  util.checker=true;
                                 _r4.getrIndex().put(krr, sp);
                              _r4.getR().put(sub, val);
                                
                                
                            }
                                 }
                                 
                            
                             }
                            
                        }
                          
                    }
                
                
                ///get the first key and get how many keys other table have, if there are more than 1
                
              
                
         
//                for(long val:_c1.getR().get(sub)){
//               
//                    SO sp =new SO(sub,val);
//                    Set<Long> in=_c1.getrIndex().getKeys(sp);
//
//                    for(long kin:in){
//                        
//                       
//                        for(long k2:keyall){
//                           if(_r2.getrIndex().getKey(new SO(k2,sub))==kin){
//                             _r4.getrIndex().put(kin, sp);
//                              _r4.getR().put(sub, val);
//                        }
//                        }
//                        
////                        
////                        
////                        
////                   
////                    }
//                }
//                
//                
//         
//                 
//                 
//                 
//                    
//                
//                
//            }
            
        }
     
        }
    }
    
    ///o for current state
  
    @Override
    public void soHashJoinCC(MultiBiMap<Long, Long> _c1,MultiBiMap<Long, Long> _c2,MultiBidirectionalIndex _r1,MultiBidirectionalIndex  _r2){
        
        for(Long o:_c1.values()){//change the order
            
            if(_c2.containsKey(o)){
                 util.checker=true;
                 util.indexes.clear();
               // _r1.getR().put(_c1.getKey(o), o);
                
                Set<Long> keyall=_c1.getKeys(o);
           
                for(long  ka:keyall){
                     _r1.getR().put(ka, o);
                      _r1.getrIndex().put(++ util.indexCount,new SO(ka, o));
                       util.indexes.add( util.indexCount);
                     // index++;
                }
                
               
                
               
                
                Set<Long> val=_c2.get(o);
                for(long v:val){
                    _r2.getR().put(o,v);
                    for(long kin: util.indexes)
                    _r2.getrIndex().put(kin,new SO(o, v));
                }
                
                
                
                
                
                ///get all the values of the with this key
                
            }
            
        }
        
        
        
      //  this.eachJoinResult(_r1, _r2,_pC,_jpC);
        
        
    }
    
   // Edit 3 
    @Override
    public void soHashJoinRC(MultiBiMap<Long, Long> _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex _r3,MultiBidirectionalIndex  _r4,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
        
        
       // if(_c1.size()<_r2.getR().size()){
         //_C1 is a join state
            for(long sub:_c1.values()){
                
                if(_r2.getR().containsKey(sub)){
                   util.indexes.clear();
                    //this.parentChild.clear();
                     util.checker=true;
                  
                    for(long val:_r2.getR().get(sub)){
                        _r4.getR().put(sub, val);
                        SO sp= new SO(sub,val);
                        Set<Long> in=_r2.getrIndex().getKeys(sp);
                        for(long kin:in){
                             util.indexes.add(kin);
                            _r4.getrIndex().put(kin, sp);
                        }
                        
                  
                        
                        
                    }
                    
                    
                    
                    
               
                    
                    Set<Long> keyall= _c1.getKeys(sub);
                    for(long ka:keyall){
                            _r3.getR().put(ka, sub);
                            for(long kin: util.indexes){
                         
                               _r3.getrIndex().put(kin, new SO(ka, sub));

                            }
                            }
                    
                    
                    
                  
                    
                    
                    
                    
                }
                
            }

        
        this.eachJoinResult(_r3, _r4,_jpC,_pC);
       // this.graphPairResult(dd);
        
    }
    
    
    
   
    @Override
    public void soHashJoinRC2(MultiBidirectionalIndex _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex _r3,MultiBidirectionalIndex  _r4){
        
        for(long sub:_c1.getR().values()){
            
            if(_r2.getR().containsKey(sub)){
          
               
                ///first get the index value from r2
              //  long index=_r2.getrIndex().getKey(new SO(sub,_r2.getR().getValue(sub)));
                Set<Long> keyall=_r2.getR().get(sub);
                Set<Long> keyall2=_c1.getR().getKeys(sub);
                for(long val:keyall){
                    
                    SO sp= new SO(sub,val);   //may be make two objects of this kind
                   Set<Long> in=_r2.getrIndex().getKeys(sp);
                   for(long kin:in){
                       
                       
                       for(long key:keyall2){
                           
                           SO sp2= new SO(key,sub);
                           for (long krr:_c1.getrIndex().getKeys(sp2)) {
                              if(krr == kin){  ///change it later
                               _r4.getrIndex().put(kin, sp); ///duplicates?
                               _r4.getR().put(sub, val);
                                 util.checker=true;
                                _r3.getrIndex().put(krr, sp2);
                                   _r3.getR().put(key, sub);
                           } 
                           }
                           
                           
                       }
                   
                       
                    
                   }
                   
                }
                
                
                
                
                
//                for(long ka:keyall2){    ///whats the point of this one????
//                      SO sp=new SO(ka, sub);
//                      
//                      
//                        Set<Long> in=_c1.getrIndex().getKeys(sp);
//                      
//                       
//                       
//                       for(long kin:in){
//                           for(long k2:keyall){    //change it later
//                               if(_r2.getrIndex().getKey(new SO(sub,k2))==kin){
//                                   _r3.getrIndex().put(kin, sp);
//                                   _r3.getR().put(ka, sub);
//                               }
//                           }
//                           
//                       }
//                        
//                       }
                }
            
                
                
            
            
        }
       // this.eachJoinResult(_r3, _r4);
    }
    
    
    ///////////////////////######################################################################////////////////////////////////////////
    
    
    
    
    
    
    
    

    
   
    @Override
    public void ooHashJoinCC(MultiBiMap<Long, Long> _c1,MultiBiMap<Long, Long> _c2,MultiBidirectionalIndex _r1,MultiBidirectionalIndex  _r2){
        
        
        for(Long obj:_c1.values()){
            if(_c2.containsValue(obj)){
                 util.indexes.clear();
                 util.checker=true;
               // _r1.getR().put(_c1.getKey(obj), obj);
              
               Set<Long> keyall= _c1.getKeys(obj);
               for(long key:keyall){
                      _r1.getR().put(key, obj);
                      _r1.getrIndex().put(++ util.indexCount, new SO(key, obj));
                       util.indexes.add( util.indexCount);
                     
               }
                
                
                
              
                Set<Long> keyall2= _c2.getKeys(obj);
                for(long key:keyall2){
                    _r2.getR().put(key, obj);
                   for(long kin: util.indexes)
                       _r2.getrIndex().put(kin, new SO(key,obj));
                }
                
                
                
                
                
            }
            
        }
        
       // this.eachJoinResult(_r1, _r2);
    }
    
    
   
    @Override
    public void ooHashJoinRC(MultiBiMap<Long, Long> _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex _r3,MultiBidirectionalIndex  _r4){
        
        
        
        if(_c1.valueSize()<_r2.getR().valueSize()){
            
            
            for(long obj:_c1.values()){
                if(_r2.getR().containsValue(obj)){
                    util.indexes.clear();
                     util.checker=true;
                    ///first get the index from the _r2
                  //  long index= _r2.getrIndex().getKey(new SO(_r2.getR().getKey(obj),obj));
                    
                  
                    Set<Long> keyall= _c1.getKeys(obj);
                    
                    
                    for(long keys:keyall){
                         _r3.getR().put(keys, obj);
                         SO sp= new SO(keys, obj);
                         
                         Set<Long> in=_r2.getrIndex().getKeys(sp);
                         for(long kin:in){
                             util.indexes.add(kin);
                               _r3.getrIndex().put(kin, sp);
                         }
                    }
                    
                   
                    
                    Set<Long> keyall2=_r2.getR().getKeys(obj);
                  for(long keys:keyall2 ){
                      _r4.getR().put(keys,obj);
                      
                      for(long kin: util.indexes)
                           _r4.getrIndex().put(kin, new SO(keys,obj));
                  }
                    
                    
                    
                   
                    
                    
                    
                    
                    
                    
                    
                }
                
                
            }
            
            
            
        }else{
            
            
            for(long obj:_r2.getR().values()){
                if(_c1.containsValue(obj)){
                     util.indexes.clear();
                     util.checker=true;
                  
                    Set<Long> keyall= _c1.getKeys(obj);
                    
                    
                    for(long keys:keyall){
                         _r3.getR().put(keys, obj);
                         SO sp= new SO(keys, obj);
                         
                         Set<Long> in=_r2.getrIndex().getKeys(sp);
                         for(long kin:in){
                              util.indexes.add(kin);
                               _r3.getrIndex().put(kin, sp);
                         }
                    }
                    
                   
                    
                    Set<Long> keyall2=_r2.getR().getKeys(obj);
                  for(long keys:keyall2 ){
                      _r4.getR().put(keys,obj);
                      
                      for(long kin: util.indexes)
                           _r4.getrIndex().put(kin, new SO(keys,obj));
                  }
                    
                    
                    
                    
                }
                
                
            }
            
            
        }
        
        
     // this.eachJoinResult(_r3, _r4);  
    }
    
    
    
    ///Issue is here some where
   
    @Override
    public void ooHashJoinRC2(MultiBidirectionalIndex _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex _r3,MultiBidirectionalIndex  _r4){
        
        ///c1 join state
        for(long obj:_c1.getR().values()){
            if(_r2.getR().containsValue(obj)){
              //  this.indexes.clear();
                
                
                ///get the keys from c1
                
                Set<Long> keyall=_c1.getR().getKeys(obj);
                
                Set<Long> keyall2= _r2.getR().getKeys(obj);
                
                for(long keys:keyall){
                     
                     SO sp=new SO(keys,obj);
                     Set<Long> in =_c1.getrIndex().getKeys(sp);
                     for(long kin:in){
                         
                          for(long val:keyall2){
                              
                                     // long index2=_r2.getrIndex().getKey(new SO(val,obj));
                                      
                                    
                                      for (long kkr: _r2.getrIndex().getKeys(new SO(val,obj))) {
                                    if(kkr==kin){
                                         _r3.getrIndex().put(kin,sp);
                                         _r3.getR().put(keys, obj);
                                            util.checker=true;
                                          _r4.getrIndex().put(kkr,new SO(val,obj));
                                         _r4.getR().put(val,obj);
                                     }
                                           }
                                      
                                    
                          }
                         
                         
                        
                         
                     }
                     
                }
                        
              
                ///first get the index from the _r2
             
                
//                for(long keys:keyall2){
//                    
//               //   for(long kin:this.indexes)
//                      
//                   
//                   SO sp= new SO(keys,obj);
//                   
//                   Set<Long> in=_r2.getrIndex().getKeys(sp);
//                   
//                   for(long kin:in){
//                       
//                       for(long k2:keyall){   ///remove this loop
//                           
//                       
//                       
//                       
//                       if(_c1.getrIndex().getKey(new SO(k2,obj))==kin){
//                            _r4.getrIndex().put(kin,sp);
//                            _r4.getR().put(keys,obj);
//                       }
//                       }
//                   }
//                }
//                
               
                
                
                
                
                
            }
            
            
        }
        
        
      // this.eachJoinResult(_r3, _r4);
      
        
    }
    
    
    @Override
    public void eachJoinResult(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _c2,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
       
     /*  System.out.println("Map 1");
        
        for (Map.Entry<Indexes, SO> entry : _c1.getrIndexUpdated().entries())
                 {
    //System.out.println(entry.getKey().toString() + "-->" + entry.getValue());
       System.out.println(entry.getKey().toString()+"--> Subject  "+ ((SO)entry.getValue()).getSub() +"--> Object " +  ((SO)entry.getValue()).getPred());
                 }
        
        for(Entry<Indexes, Indexes> entry:_pC.entries()){
              System.out.println("Parent "+entry.getKey()+" Child "+ entry.getValue());
        }
        
        System.out.println("Map 2");
        
        for (Map.Entry<Indexes, SO> entry : _c2.getrIndexUpdated().entries())
                 {
  //  System.out.println(entry.getKey().toString() + "-->" + entry.getValue());
     System.out.println(entry.getKey().toString()+"--> Subject  "+ ((SO)entry.getValue()).getSub() +"--> Object " +  ((SO)entry.getValue()).getPred());
                 }
         for(Entry<Indexes, Indexes> entry:_jpC.entries()){
              System.out.println("Parent "+entry.getKey()+" Child "+ entry.getValue());
        }
        
       
        System.out.println("#####################################");
       */
    }
    
    
    
    @Override
    public void graphPairResult(Multimap<Long,Long> m){
         System.out.println("Iteration");
        for (long key : m.keySet()) { 
           
            
            System.out.println("From " + key +" To "+m.get(key));
        }
          System.out.println("#####################################");
    }
    
    
    
    
    @Override
           public void outputIndex( AutomataState s){
       
            System.out.println(s.getRule().getPredicate().getPredicateValue());
              MultiBiMap<Long,SO> vv=null;
            if(!s.getEdge().getRreal().getR().isEmpty()){
                 vv=s.getEdge().getRreal().getrIndex();
            }else{
                vv=s.getEdge().getrCopied().getrIndex();
            }
           
            for (Map.Entry<Long,SO> entry : vv.entries())
                 {
    System.out.println(entry.getKey() + "  ==>  " + entry.getValue().getSub()+"  "+  entry.getValue().getPred());
                 }
            
            System.out.println("###########################");
        
    }
  
        
    @Override
           public void outputMaterialsedview( AutomataState s){
       
            System.out.println(s.getRule().getPredicate().getPredicateValue());
              MultiBiMap<Long,Long> vv=null;
            if(!s.getEdge().getRreal().getR().isEmpty()){
                 vv=s.getEdge().getRreal().getR();
            }else{
                vv=s.getEdge().getrCopied().getR();
            }
           
            for (Map.Entry<Long, Long> entry : vv.entries())
                 {
    System.out.println(entry.getKey() + "," + entry.getValue());
                 }
            
            System.out.println("###########################");
        
    }
}
